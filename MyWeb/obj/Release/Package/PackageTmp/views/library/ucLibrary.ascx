﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLibrary.ascx.cs" Inherits="MyWeb.views.library.ucLibrary" %>
<link href="../../theme/default/plugins/lightSlider/lightslider.css" rel="stylesheet" />
<script src="../../theme/default/plugins/lightSlider/lightslider.js"></script>
<script>
    $(document).ready(function () {
        $("#content-slider").lightSlider({
            loop: true,
            keyPress: true
        });
        $('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 9,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            onSliderLoad: function () {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>
<div class="box-library">
    <asp:Literal runat="server" ID="ltrAdv" Visible="True"></asp:Literal>    
    <asp:Literal ID="ltrLibrary" runat="server"></asp:Literal> 
</div>

<script>
    $(document).ready(function () {        
        $(".group-box").colorbox({ rel: 'group1' });
    });
</script>
