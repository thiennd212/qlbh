﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProMotionV2.ascx.cs" Inherits="MyWeb.views.products.ucProMotionV2" %>
<%if (showAdv)
  {%>
<div class="box-product-motion">
    <div class="header">
        <%=MyWeb.Global.GetLangKey("pro_sl_km") %>
    </div>
    <ul class="product-list">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>