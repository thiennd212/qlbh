﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProSale.ascx.cs" Inherits="MyWeb.views.products.ucProSale" %>
<%if (showAdv)
  {%>
<div class="box-pro-sale">
    <div class="header"><%= MyWeb.Global.GetLangKey("pro_best_sale")%></div>
    <ul class="body-pro">
        <marquee onmouseover="this.stop();" onmouseout="this.start();" direction="up" height="243" behavior="scroll" scrollamount="2">
            <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
        </marquee>
    </ul>
</div>
<%} %>
