﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMenuMain.ascx.cs" Inherits="MyWeb.views.control.ucMenuMain" %>
<div class="nav-main">
    <asp:Literal ID="ltrMainMenu" runat="server"></asp:Literal>
</div>
<div class="box-marque">
    <asp:Literal ID="ltrMarquee" runat="server"></asp:Literal>
</div>