﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="billList.ascx.cs" Inherits="MyWeb.control.panel.bill.billList" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý vận đơn</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý vận đơn</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách vận đơn</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa thư mục đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>
                           <%-- <div class="col-sm-2">
                                <asp:Label runat="server" ID="lblMaVanDon" class="form-control">Mã vận đơn:</asp:Label>
                            </div>--%>
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Mã đơn hàng"></asp:TextBox>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th width="100">Ngày tháng</th>
                                                <th width="100">Mã đơn hàng</th>
                                                <th width="200">Diễn giải</th>
                                                <th width="150">Nơi đi</th>
                                                <th width="150">Nơi đến</th>
                                                <th width="80">Tình trạng</th>
                                                <th>Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"id")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#ConvertDateToString(DataBinder.Eval(Container.DataItem, "NgayThang"))%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "Code")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "Description")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "AddressFrom")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "AddressTo")%>
                                                        </td>
                                                        <td>
                                                            <%#GetStatus(Eval("Status").ToString())%>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"id")%>' CommandName="Edit" ToolTip="Xem đơn"><i class="fa fa-pencil-square-o"></i>Xem</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa vận đơn</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                    <div class="panel-heading p-0">
                        <div class="panel-heading-btn m-r-10 m-t-10">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        </div>
                        <div class="tab-overflow">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin vận đơn</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="tab-pane fade active in" id="info-tab">
                            <div class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Mã đơn hàng:</label>
                                    <div class="col-md-7">
                                        <asp:HiddenField ID="hidID" runat="server" />
                                        <asp:TextBox ID="txtMaVanDon" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nội dung:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtDescription" runat="server" class="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nơi đi:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtAddressFrom" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nơi đến:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtAddressTo" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Ngày tháng:</label>
                                    <div class="col-md-3">
                                        <div class="input-group date datepicker-autoClose">
                                        <asp:TextBox ID="txtNgay" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Trạng thái:</label>
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="drlStatus" runat="server" class="form-control">                                            
                                            <asp:ListItem Value="1">Đang chuyển</asp:ListItem>
                                            <asp:ListItem Value="2">Đã chuyển</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Trạng thái thanh toán:</label>
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="drlStatusTT" runat="server" class="form-control">                                            
                                            <asp:ListItem Value="1">Chưa thanh toán</asp:ListItem>
                                            <asp:ListItem Value="2">Đã thanh toán</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-horizontal form-bordered">
                            <div class="form-group" style="border-top: 1px solid #eee;">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </asp:Panel>
</div>
