﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="view_product.aspx.cs" Inherits="MyWeb.view_product" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="view_products" runat="server">
        <div class="pro-detail-view">
            <asp:HiddenField ID="hidID" runat="server" />
            <asp:Repeater ID="rptpro" runat="server">
                <ItemTemplate>
                    <div class="form-left">
                        <%#BindImageSlide(Eval("proImage").ToString(), Eval("proName").ToString(),"1")%>
                        <%#BindImageSlide(Eval("proImage").ToString(), Eval("proName").ToString(),"2")%>
                    </div>
                    <div class="form-right">
                        <h4 class="color-text-head"><%#DataBinder.Eval(Container.DataItem, "proName")%></h4>
                        <div class="text-line">
                            <% = MyWeb.Global.GetLangKey("view_fast1") %>: <%#Eval("proCode") %>
                            <div class="addthis">
                                <div class="addthis_native_toolbox"></div>
                            </div>
                        </div>
                        <div class="text-price"><%=MyWeb.Global.GetLangKey("product_price_sale") %>: <%#BindSalePrice(Eval("proPrice").ToString(), Eval("proOriginalPrice").ToString(), MyWeb.Global.GetLangKey("currency_symbol")) %> ( <%#Status(Eval("proStatus").ToString()) %> )</div>
                        <div class="info-sort">
                            <%#DataBinder.Eval(Container.DataItem, "proWarranty")%>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <div class="form-right">
                <div class="text-line">
                    <div class="row">
                        <div class="col-lg-4">
                            <%=MyWeb.Global.GetLangKey("cart_amount") %><br/>
                            <input type="number" class="input-card boder-color" value="1" id="proQuantity" min="1" max="1000" />
                        </div>
                        <div class="col-lg-4">
                            <%=MyWeb.Global.GetLangKey("cart_money") %>:<br/>
                            <span class="price-total color-text-second"><span id="total"></span></span>
                        </div>
                    </div>
                </div>

                <div class="text-line">
                    <div class="row">
                        <div class="col-lg-4">
                            <asp:LinkButton ID="btnAddNow" runat="server" class="btn-add-card" OnClick="btnAddNow_Click"><%=MyWeb.Global.GetLangKey("pro_button_order1")%></asp:LinkButton>
                        </div>
                        <div class="col-lg-4">
                            <asp:LinkButton ID="btnAdd" runat="server" class="btn-add-card order-now" OnClick="btnAdd_Click"><%=MyWeb.Global.GetLangKey("pro_button_order2")%></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="text" id="idsoluong3" value="1" runat="server" hidden="hidden" />
        <script>
            $("#total")[0].innerHTML = $(".text-price").find("span").text();
            $("#proQuantity").on("change keyup", function () {
                var soluong = $('#proQuantity').val();
                if (soluong > 1000) {
                    $("#proQuantity").val('1000');
                    $("input[id$='idsoluong3").val('1000');
                    soluong = 1000;
                }
                $("input[id$='idsoluong3").val(soluong);

                var price = $("#proQuantity").val();
                $("#ctl14_ucLoadControl_ctl00_hidID").val(price);
                var money = $(".text-price").find("span").text();
                var cur = money.substring((money.indexOf(" ") + 1));
                var quantity = $("#proQuantity").val();
                money = money.substring(0, money.indexOf(" "));
                var arr = money.split(",");
                var total = 0;
                for (var i = 0 ; i <= (arr.length - 1) ; i++) {
                    //if (i < (arr.length - 1)) { total += (arr[i] * 1000) * quantity; }
                    //else total += (arr[i] * quantity);
                    money = money.replace(',', '');
                }
                money = parseFloat(money);
                total = money * quantity;
                total = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
                $("#total")[0].innerHTML = total + " " + cur;

            });
        </script>

        <%if (showjs)
          {%>
        <script type="text/javascript">

            $(document).ready(function () {
                var sync1 = $("#sync1");
                var sync2 = $("#sync2");

                sync1.owlCarousel({
                    singleItem: true,
                    slideSpeed: 1000,
                    navigation: false,
                    pagination: false,
                    afterAction: syncPosition,
                    responsiveRefreshRate: 200,
                });

                sync2.owlCarousel({
                    items: 5,
                    itemsDesktop: [1199, 5],
                    itemsDesktopSmall: [979, 10],
                    itemsTablet: [768, 8],
                    itemsMobile: [479, 4],
                    navigation: true,
                    navigationText: ["«", "»"],
                    rewindNav: false,
                    scrollPerPage: false,
                    slideSpeed: 1500,
                    pagination: false,
                    paginationNumbers: false,
                    autoPlay: false,
                    afterInit: function (el) {
                        el.find(".owl-item").eq(0).addClass("synced");
                    }
                });

                function syncPosition(el) {
                    var current = this.currentItem;
                    $("#sync2")
                      .find(".owl-item")
                      .removeClass("synced")
                      .eq(current)
                      .addClass("synced")
                    if ($("#sync2").data("owlCarousel") !== undefined) {
                        center(current)
                    }
                }

                $("#sync2").on("click", ".owl-item", function (e) {
                    e.preventDefault();
                    var number = $(this).data("owlItem");
                    sync1.trigger("owl.goTo", number);
                });

                function center(number) {
                    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                    var num = number;
                    var found = false;
                    for (var i in sync2visible) {
                        if (num === sync2visible[i]) {
                            var found = true;
                        }
                    }

                    if (found === false) {
                        if (num > sync2visible[sync2visible.length - 1]) {
                            sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                        } else {
                            if (num - 1 === -1) {
                                num = 0;
                            }
                            sync2.trigger("owl.goTo", num);
                        }
                    } else if (num === sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", sync2visible[1])
                    } else if (num === sync2visible[0]) {
                        sync2.trigger("owl.goTo", num - 1)
                    }

                }

            });
        </script>
        <script type="text/javascript">
            CloudZoom.quickStart();
        </script>
        <%} %>
    </form>
</body>
</html>
