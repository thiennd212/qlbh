﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.control
{
    public partial class ucMenu : System.Web.UI.UserControl
    {
        static string lang = "vi";
        string _strAdmin = "";
        string _strUserid = "";
        string[] _strAuthorized = null;
        string _strUserRole = "";
        HttpCookie _interface = null;
        public string strFaceValue = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _strAdmin = Session["admin"].ToString(); }
            if (Session["uid"] != null) _strUserid = Session["uid"].ToString();
            try
            {
                if (Session["strAuthorized"] != null) _strAuthorized = Session["strAuthorized"].ToString().Split(';').ToArray();
            }
            catch
            {
                _strAuthorized = null;
            }

            if (Session["UserRole"] != null) _strUserRole = Session["UserRole"].ToString();

            _interface = Request.Cookies["interface"];
            if (_interface == null)
            {
                strFaceValue = "MENU_LEFT";
            }
            else
            {
                strFaceValue = _interface.Value.ToString();
            }

            if (!IsPostBack)
            {
                var strID = Session["LangAdm"].ToString();
                var objLang = db.tbLanguages.Where(u => u.lanId == strID).FirstOrDefault();
                string str = "";
                str += "<img src=\"" + objLang.lanImage + "\" title=\"" + objLang.lanName + "\" alt=\"" + objLang.lanName + "\" />";
                //ltrLang.Text = str;
                BindData();
            }
        }

        void BindData()
        {
            string _strMenu = "";
            var objMenu = db.tbModules.Where(s => s.active == true && s.treecode.Length == 5).OrderBy(s => s.treecode).ToList();

            if (_strUserRole == "2" && _strAuthorized != null)
            {
                objMenu = objMenu.Where(s => _strAuthorized.Contains(s.treecode)).OrderBy(x => x.treecode).ToList();
            }

            var strUrl = Request.Url.AbsoluteUri;
            int intNo = 1;
            if (objMenu.Count() > 0)
            {
                foreach (tbModule obj in objMenu)
                {
                    var objSub = db.tbModules.Where(s => s.active == true && s.treecode.StartsWith(obj.treecode) && s.treecode.Length > 5).OrderBy(x => x.treecode).ToList();
                    if (_strUserRole == "2" && _strAuthorized != null)
                    {
                        objSub = objSub.Where(s => _strAuthorized.Contains(s.treecode)).OrderBy(x => x.treecode).ToList();
                    }
                    
                    if (objSub.Count() > 0)
                    {
                        if (strUrl.Contains(obj.tagsname))
                        {
                            _strMenu += "<li class=\"has-sub active\">";
                        }
                        else
                        {
                            _strMenu += "<li class=\"has-sub\">";
                        }
                        _strMenu += "<a href=\"javascript:;\">";
                        _strMenu += "<b class=\"caret pull-right\"></b>";
                        _strMenu += obj.icon;
                        _strMenu += "<span>" + obj.name + "</span>";
                        _strMenu += "</a>";

                        _strMenu += "<ul class=\"sub-menu\">";
                        foreach (tbModule objS in objSub)
                        {
                            _strMenu += "<li><a href=\"" + objS.link + "\">" + objS.name + "</a></li>";
                        }
                        _strMenu += "</ul>";
                        _strMenu += "</li>";
                    }
                    else
                    {
                        if (strUrl.Contains(obj.tagsname))
                        {
                            _strMenu += "<li class=\"active\">";
                        }
                        else
                        {
                            _strMenu += "<li>";
                        }
                        if (obj.link == "")
                        {
                            _strMenu += "<a href=\"javascript:;\">";
                        }
                        else
                        {
                            _strMenu += "<a href=\"" + obj.link + "\" title=\"" + obj.name + "\">";
                        }

                        _strMenu += obj.icon;
                        _strMenu += "<span>" + obj.name + "</span>";
                        _strMenu += "</a>";
                        _strMenu += "</li>";
                    }

                    if (intNo == 5 || intNo == 10)
                    {
                        _strMenu += "<li style=\"height: 15px;\"></li>";
                    }

                    intNo += 1;
                }

                //_strMenu += "<li style=\"height: 15px;\"></li>";

            }
            ltrMenu.Text = _strMenu;
        }

        protected void btnLinkVi_Click(object sender, EventArgs e)
        {
            Session["LangAdm"] = "vi";
            MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
            var langname = db.tbLanguages.Where(l => l.lanId == "vi").Select(s => s.lanName);
            Session["lanname"] = langname.FirstOrDefault();
            Response.Redirect("/control.panel/");
        }

        protected void btnLinkEn_Click(object sender, EventArgs e)
        {
            Session["LangAdm"] = "en";
            MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
            var langname = db.tbLanguages.Where(l => l.lanId == "en").Select(s => s.lanName);
            Session["lanname"] = langname.FirstOrDefault();
            Response.Redirect("/control.panel/");
        }
    }
}