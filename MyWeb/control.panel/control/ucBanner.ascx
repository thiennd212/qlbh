﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBanner.ascx.cs" Inherits="MyWeb.control.panel.control.ucBanner" %>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="/control.panel/" class="navbar-brand">
                <img src="../theme/admin_cms/img/logo_cms_ad.png"/>
            </a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li class="hidden">
                <div class="navbar-form full-width">
                    <div class="form-group">
                        <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                        <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn btn-search"><i class="fa fa-search"></i></asp:LinkButton>
                    </div>
                </div>
                <!-- end search -->
            </li>
            <li class="dropdown hidden">
                <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                    <i class="fa fa-bell-o"></i>
                    <span class="label">
                        <asp:Literal ID="ltrOrderNew" runat="server"></asp:Literal></span>
                </a>
                <ul class="dropdown-menu media-list pull-right animated fadeInDown">
                    <li class="dropdown-header">Thông báo đơn hàng
                        <asp:Literal ID="ltrNumberOrder" runat="server"></asp:Literal>

                    </li>
                    <asp:Repeater ID="rptOrderList" runat="server">
                        <ItemTemplate>
                            <li class="media">
                                <a href='/admin-order/list.aspx?id=<%#DataBinder.Eval(Container.DataItem,"id")%>'>
                                    <div class="media-left">
                                        <i class="media-object bg-red">
                                            <asp:Label ID="lblorder" runat="server"></asp:Label></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Đơn hàng của <%#Eval("name").ToString()%></h6>
                                        <div class="text-muted f-s-11"><%#FormatDate(DataBinder.Eval(Container.DataItem, "createdate").ToString())%></div>
                                    </div>
                                </a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <li class="dropdown-footer text-center">
                        <a href="/admin-order/list.aspx">Xem tất cả</a>
                    </li>
                </ul>
            </li>
            <!-- end đơn hàng -->
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../theme/admin_cms/img/user.png" alt="" />
                    <span class="hidden-xs">Xin chào:
                        <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="/admin-system/myacount.aspx">Thay đổi thông tin</a></li>
                    <li><a href="/admin-system/changepass.aspx">Đổi mật khẩu</a></li>
                    <li class="hidden"><a href="/admin-system/configure.aspx">Cài đặt</a></li>
                    <li class="divider"></li>
                    <li>
                        <asp:LinkButton ID="btnLogout" runat="server" OnClick="btnLogout_Click">Thoát</asp:LinkButton></li>
                </ul>
            </li>
            <li class="view-site hidden">
                <a href="/" target="_blank"><i class="fa fa-paper-plane"></i> Xem website</a>
            </li>
            <!-- end user -->
        </ul>
        <!-- end header navigation right -->
    </div>
</div>
<!--  end header -->
<script>
    $("$<%=txtSearch.ClientID%>").keydown(function (event) {
        if (event.keyCode == 13) {
            $("$<%=btnSearch.ClientID%>").focus();
            $("$<%=btnSearch.ClientID%>").click();
        }
    });

    function clickButton(e, buttonid) {
        var evt = e ? e : window.event;
        var bt = document.getElementById(buttonid);

        if (bt) {
            if (evt.keyCode == 13) {
                bt.click();
                return false;
            }
        }
    }
</script>
