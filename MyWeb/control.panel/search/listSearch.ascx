﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listSearch.ascx.cs" Inherits="MyWeb.control.panel.search.listSearch" %>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Kết quả tìm kiếm</li>
    </ol>
    <h1 class="page-header">Kết quả tìm kiếm</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse" data-sortable-id="ui-media-object-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Danh sách sản phẩm</h4>
                </div>

                <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                    <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                    <button class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="alert alert-info fade in" style="display: none;" id="frame_messenger">
                    <span id="div_messenger"></span>
                    <button class="close" data-dismiss="alert" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                <thead>
                                    <tr>
                                        <th width="10">
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox></th>
                                        <th width="50">Ảnh</th>
                                        <th width="80">Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th width="100">Nhóm</th>
                                        <th width="80">Giá bán</th>
                                        <th width="50">Sắp xếp</th>
                                        <th width="20"></th>
                                        <th width="150">Công cụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptProductList" runat="server" OnItemCommand="rptProductList_ItemCommand">
                                        <ItemTemplate>
                                            <tr class="even gradeA">
                                                <td>
                                                    <asp:CheckBox ID="chkBox" runat="server"></asp:CheckBox>
                                                    <asp:HiddenField ID="hidProID" runat="server" Value='<%#Eval("proId")%>' />
                                                </td>
                                                <td><%#BindImages(DataBinder.Eval(Container.DataItem, "proImage").ToString())%></td>
                                                <td>
                                                    <%#DataBinder.Eval(Container.DataItem, "proCode").ToString()%>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" class='<%#DataBinder.Eval(Container.DataItem, "proId")+"_txt_proName form-control input-sm"%>' ID="txtProName" Text='<%#DataBinder.Eval(Container.DataItem, "proName").ToString()%>'></asp:TextBox>
                                                </td>
                                                <td>
                                                    <%#BindCateName(DataBinder.Eval(Container.DataItem, "catId").ToString())%>
                                                </td>
                                                <td style="text-align: right">
                                                    <asp:TextBox ID="txtPrice" Text='<%#BindNumber(DataBinder.Eval(Container.DataItem, "proPrice").ToString())%>' class='<%#DataBinder.Eval(Container.DataItem, "proId")+"_txt_proPrice form-control input-sm"%>' runat="server" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOrder" Text='<%#DataBinder.Eval(Container.DataItem, "proOrd")%>' runat="server" class='<%#DataBinder.Eval(Container.DataItem, "proId")+"_txt_proOrder form-control input-sm"%>' onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "proActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "proActive").ToString())%></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Edit" class="btn btn-success btn-xs" ToolTip="Sửa sản phẩm"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                    <asp:LinkButton ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="DEL" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');" class="btn btn-danger btn-xs" ToolTip="Xóa sản phẩm"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row dataTables_wrapper">
                        <div class="col-sm-5">
                            <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                            </div>
                        </div>

                        <div class="col-sm-7">
                            <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination">
                                    <li id="data-table_previous" class="paginate_button previous disabled">
                                        <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                    </li>
                                    <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                            <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <li id="data-table_next" class="paginate_button next">
                                        <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
