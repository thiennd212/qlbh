﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb;

namespace MyWeb.control.panel.bill
{
    public partial class billList : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {

            var _listForder = db.tbOrders.Where(x => x.Lang == lang).OrderBy(s => s.id).ToList();

            if (txtSearch.Text != "")
            {
                _listForder = _listForder.Where(s => s.Code.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        public string ConvertDateToString(object ngaythang)
        {
            DateTime nt = Convert.ToDateTime(ngaythang);
            return nt.ToString("HH:mm MM/dd/yyyy",
                                CultureInfo.InvariantCulture);
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    tbOrdersDB.tbOrders_Delete(strID);
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    break;
                case "Edit":
                    List<tbOrder> list = db.tbOrders.Where(x => x.id == int.Parse(strID)).ToList();
                    hidID.Value = strID;
                    txtMaVanDon.Text = list[0].Code;
                    txtDescription.Text = list[0].Description;
                    txtAddressFrom.Text = list[0].AddressFrom;
                    txtAddressTo.Text = list[0].AddressTo;
                    txtNgay.Text = string.Format("{0:MM/dd/yyyy hh:mm t}", list[0].NgayThang);
                    drlStatus.SelectedValue = list[0].Status.ToString();
                    drlStatusTT.SelectedValue = list[0].StatusTT.ToString();
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected DateTime convertDate(string date)
        {
            DateTime datetime = DateTime.Now;
            if (date != "")
            {
                var arrDate = date.Split(' ');
                string strNewTime = arrDate[1];
                if (arrDate[2] == "PM")
                {
                    var hour = int.Parse(arrDate[1].Split(':')[0].ToString());
                    hour = hour + 12;
                    strNewTime = hour + ":" + arrDate[1].Split(':')[1];
                }
                else
                {
                    var hour = int.Parse(arrDate[1].Split(':')[0].ToString());
                    if (hour < 10)
                        strNewTime = "0" + hour + ":" + arrDate[1].Split(':')[1];
                    else
                        strNewTime = hour + ":" + arrDate[1].Split(':')[1];
                }
                string strNewDate = arrDate[0] + " " + strNewTime + ":00";
                datetime = DateTime.ParseExact(strNewDate, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
            }
            return datetime;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            tbOrder itemOrder = new tbOrder();
            var curId = hidID.Value;

            if (curId.Length > 0)
            {
                itemOrder = db.tbOrders.FirstOrDefault(s => s.id == int.Parse(curId));
            }

            itemOrder.Code = txtMaVanDon.Text.Trim();
            itemOrder.Description = txtDescription.Text;
            itemOrder.AddressFrom = txtAddressFrom.Text;
            itemOrder.AddressTo = txtAddressTo.Text;
            itemOrder.Status = int.Parse(drlStatus.SelectedValue);
            itemOrder.StatusTT = int.Parse(drlStatusTT.SelectedValue);
            itemOrder.Lang = "vi";
            if (!string.IsNullOrEmpty(txtNgay.Text))
            {
                itemOrder.NgayThang = convertDate(txtNgay.Text);
            }
            else
            {
                itemOrder.NgayThang = DateTime.Now;
            }

            if (curId.Length == 0)
            {
                db.tbOrders.InsertOnSubmit(itemOrder);
                db.SubmitChanges();
            }
            else
            {
                db.SubmitChanges();
            }

            pnlErr.Visible = true;
            ltrErr.Text = curId.Length == 0 ? "Thêm mới thành công" : "Cập nhật thành công !";
            Resetcontrol();
            BindData();
            hidID.Value = "";
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            hidID.Value = "";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            txtNgay.Text = string.Format("{0:MM/dd/yyyy hh:mm t}", DateTime.Now);
            Resetcontrol();
        }

        void Resetcontrol()
        {
            hidID.Value = "";
            txtMaVanDon.Text = "";
            txtDescription.Text = "";
            txtAddressFrom.Text = "";
            txtAddressTo.Text = "";
            drlStatus.SelectedIndex = 0;
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        try
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("lblID");
                            var itemDel = db.tbOrders.Where(u => u.id == Convert.ToInt32(hidID.Value)).ToList();
                            if (itemDel.Count() > 0)
                            {
                                db.tbOrders.DeleteAllOnSubmit(itemDel);
                                db.SubmitChanges();
                            }
                        }
                        catch { }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected string GetStatus(string status)
        {
            string result = "";
            if (status == "0")
            {
                result = "Chưa chuyển";
            }
            else if (status == "1")
            {
                result = "Đang chuyển";
            }
            else if (status == "2")
            {
                result = "Đã chuyển";
            }

            return result;
        }
    }
}