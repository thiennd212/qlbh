﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.library
{
    public partial class libraryList : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }
        }
        void BindData()
        {
            IEnumerable<Library> _listNews = db.Libraries.Where(s => s.Lang.Trim().ToLower() == lang).OrderBy(s => s.Ord).ThenByDescending(s => s.Id);
            if (txtSearch.Text != "")
            {
                _listNews = _listNews.Where(s => s.Name.Contains(txtSearch.Text)).ToList();
            }

            if (drlForder.SelectedValue != "0")
            {
                _listNews = _listNews.Where(s => s.GroupLibraryId == int.Parse(drlForder.SelectedValue)).ToList();
            }


            if (_listNews.Count() > 0)
            {
                recordCount = _listNews.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listNews.Skip(currentPage * pageSize).Take(pageSize);
                rptListNews.DataSource = result;
                rptListNews.DataBind();
                BindPaging();
            }
        }

        void BindForder()
        {
            string str = "";
            List<tbPage> dbgrlib = new List<tbPage>();
            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {
                dbgrlib = db.tbPages.Where(s => s.pagLang.Trim() == lang && s.pagType == int.Parse(pageType.GL)).OrderBy(s => s.paglevel).ToList();
            }
            else
            {
                dbgrlib = db.ExecuteQuery<tbPage>("select * from tbPage where pagType = '300' and pagId in (select roleidmodule from tbUserRole where roleuserid=" + userid + ")").OrderBy(c => c.paglevel).ToList();
            }

            drlForder.Items.Clear();
            ddlForder.Items.Clear();
            drlForder.Items.Add(new ListItem("- Nhóm chuyên mục -", "0"));
            ddlForder.Items.Add(new ListItem("- Nhóm chuyên mục -", "0"));
            foreach (tbPage list in dbgrlib)
            {
                str = "";
                for (int j = 1; j < list.paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                drlForder.Items.Add(new ListItem(str.ToString() + list.pagName, list.pagId.ToString()));
                ddlForder.Items.Add(new ListItem(str.ToString() + list.pagName, list.pagId.ToString()));
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        public string BindCateName(string id)
        {
            tbPage dbgrlib = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(id));
            return dbgrlib != null ? dbgrlib.pagName : "";
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            TextBox txtName = (TextBox)sender;
            var blbID = (Label)txtName.FindControl("lblID");
            var obj = db.Libraries.FirstOrDefault(s => s.Id == int.Parse(blbID.Text));
            if (obj != null)
            {

                obj.Name = txtName.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên nhóm thành công !";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptListNews.Items.Count; i++)
            {
                item = rptListNews.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        string tagName = db.Libraries.Where(s => s.Id == int.Parse(hidID.Value)).FirstOrDefault().TagName;
                        string pagId = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault().pagId.ToString();
                        tbPageDB.tbPage_Delete(pagId);
                        Library dblibdelete = db.Libraries.FirstOrDefault(s => s.Id == Int32.Parse(hidID.Value));
                        db.Libraries.DeleteOnSubmit(dblibdelete);
                        db.SubmitChanges();
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công thư viện !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void rptListNews_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    var lib = db.Libraries.FirstOrDefault(s => s.Id == int.Parse(strID));
                    if (lib != null)
                    {
                        if (lib.Active == 0) { lib.Active = 1; } else { lib.Active = 0; }

                    }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "NoiBat":
                    var libNB = db.Libraries.FirstOrDefault(s => s.Id == int.Parse(strID));
                    if (libNB != null)
                    {
                        if (libNB.Priority == 0) { libNB.Priority = 1; } else { libNB.Priority = 0; }

                    }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật nổi bật thành công !";
                    break;
                case "Del":
                    string tagName = db.Libraries.Where(s => s.Id == int.Parse(strID)).FirstOrDefault().TagName;
                    string pagId = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault().pagId.ToString();
                    tbPageDB.tbPage_Delete(pagId);
                    Library dblibdelete = db.Libraries.FirstOrDefault(s => s.Id == Int32.Parse(strID));
                    db.Libraries.DeleteOnSubmit(dblibdelete);
                    db.SubmitChanges();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
                    BindData();
                    break;
                case "Edit":
                    hidID.Value = strID;
                    Library dblib = db.Libraries.FirstOrDefault(s => s.Id == Int32.Parse(strID));
                    BindForder();
                    ddlForder.SelectedValue = dblib.GroupLibraryId.ToString();
                    txtTieudetin.Text = dblib.Name;
                    txtImage.Text = dblib.Image;
                    txtVideo.Text = dblib.File;
                    txtTieudetitle.Text = dblib.Title;
                    txtDesscription.Text = dblib.Description;
                    txtKeyword.Text = dblib.Keyword;
                    chkActive.Checked = dblib.Active == 1 ? true : false;
                    chkNoiBat.Checked = dblib.Priority == 1 ? true : false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        bool Validation()
        {
            if (ddlForder.SelectedValue == "0") { ltrErr2.Text = "Chưa chọn thuộc nhóm chuyên mục !!"; ddlForder.Focus(); pnlErr2.Visible = true; return false; }
            if (txtTieudetin.Text == "") { ltrErr2.Text = "Chưa nhập tiêu đề !!"; pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string strID = hidID.Value;
                if (strID.Length == 0)
                {
                    Library dblib = new Library();
                    dblib.Name = txtTieudetin.Text.Trim();
                    dblib.GroupLibraryId = Int32.Parse(ddlForder.SelectedValue);
                    dblib.Image = txtImage.Text;
                    dblib.File = txtVideo.Text;
                    dblib.Title = txtTieudetitle.Text;
                    dblib.Description = txtDesscription.Text;
                    dblib.Keyword = txtKeyword.Text;
                    dblib.Active = chkActive.Checked ? 1 : 0;
                    dblib.Priority = chkNoiBat.Checked ? 1 : 0;
                    dblib.Id = Int32.Parse(ddlForder.SelectedValue);
                    dblib.MemberId = Int32.Parse(userid);
                    dblib.Lang = lang;
                    dblib.Ord = 1;

                    #region Page
                    tbPageDATA objNewMenu = new tbPageDATA();
                    objNewMenu.pagName = txtTieudetin.Text;
                    objNewMenu.pagImage = "";
                    objNewMenu.pagType = pageType.LD;
                    var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                    objNewMenu.paglevel = curNewType.paglevel + "00000";
                    objNewMenu.pagTitle = txtTieudetitle.Text;
                    objNewMenu.pagDescription = txtDesscription.Text;
                    objNewMenu.pagKeyword = txtKeyword.Text;
                    objNewMenu.pagOrd = "0";
                    objNewMenu.pagActive = chkActive.Checked ? "0" : "1";
                    objNewMenu.pagLang = lang;
                    objNewMenu.pagDetail = "";
                    objNewMenu.pagLink = "";
                    objNewMenu.pagTarget = "";
                    objNewMenu.pagPosition = "0";
                    objNewMenu.catId = "";
                    objNewMenu.grnId = "";
                    objNewMenu.pagTagName = "";
                    objNewMenu.check1 = "";
                    objNewMenu.check2 = "";
                    objNewMenu.check3 = "";
                    objNewMenu.check4 = "";
                    objNewMenu.check5 = "";
                    objNewMenu.pagIcon = "";
                    string tagName = MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);
                    if (tbPageDB.tbPage_Add(objNewMenu))
                    {
                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        curItem[0].pagId = curItem[0].pagId;
                        curItem[0].pagTagName = hasTagName != null ? tagName + "-" + curItem[0].pagId : tagName;
                        curItem[0].check1 = "";
                        curItem[0].check2 = "";
                        curItem[0].check3 = "";
                        curItem[0].check4 = "";
                        curItem[0].check5 = "";
                        tbPageDB.tbPage_Update(curItem[0]);

                        dblib.TagName = curItem[0].pagTagName;
                        db.Libraries.InsertOnSubmit(dblib);
                        db.SubmitChanges();

                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới thư viện thành công !";
                        Resetcontrol();
                        BindData();
                        hidID.Value = "";

                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                    #endregion
                }

                else
                {
                    Library dblib = db.Libraries.FirstOrDefault(s => s.Id == Int32.Parse(strID));
                    if (dblib != null)
                    {
                        dblib.Id = Int32.Parse(strID);
                        dblib.Name = txtTieudetin.Text.Trim();
                        dblib.GroupLibraryId = Int32.Parse(ddlForder.SelectedValue);
                        dblib.Image = txtImage.Text;
                        dblib.File = txtVideo.Text;
                        dblib.Title = txtTieudetitle.Text;
                        dblib.Description = txtDesscription.Text;
                        dblib.Keyword = txtKeyword.Text;
                        dblib.Active = chkActive.Checked ? 1 : 0;
                        dblib.Priority = chkNoiBat.Checked ? 1 : 0;
                        dblib.MemberId = Int32.Parse(userid);
                        dblib.Lang = lang;
                       
                        #region Page
                        List<tbPageDATA> curPage = tbPageDB.tbPage_GetByTagName(dblib.TagName);
                        if (curPage != null)
                        {
                            string bfTagname = curPage[0].pagTagName;
                            curPage[0].pagName = txtTieudetin.Text;
                            curPage[0].pagImage = "";
                            curPage[0].pagType = pageType.LD;
                            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                            curPage[0].paglevel = curNewType.paglevel + "00000";
                            curPage[0].pagTitle = txtTieudetitle.Text;
                            curPage[0].pagDescription = txtDesscription.Text;
                            curPage[0].pagKeyword = txtKeyword.Text;
                            curPage[0].pagOrd = "0";
                            curPage[0].pagActive = chkActive.Checked ? "0" : "1";
                            curPage[0].pagLang = lang;
                            curPage[0].pagDetail = "";
                            curPage[0].pagLink = "";
                            curPage[0].pagTarget = "";
                            curPage[0].pagPosition = "0";
                            curPage[0].catId = "0";
                            curPage[0].grnId = "0";
                            curPage[0].pagTagName = "";
                            string tagName = MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);
                            var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                            curPage[0].pagTagName = hasTagName != null ? tagName + "-" + curPage[0].pagId : tagName;
                            curPage[0].check1 = "";
                            curPage[0].check2 = "";
                            curPage[0].check3 = "";
                            curPage[0].check4 = "";
                            curPage[0].check5 = "";
                            dblib.TagName = curPage[0].pagTagName;
                            if (tbPageDB.tbPage_Update(curPage[0]))
                            {
                                #region Update các menu liên kết đến
                                List<tbPageDATA> lstUpdate = tbPageDB.tbPage_GetByAll(lang);
                                lstUpdate = lstUpdate.Where(s => s.pagLink == bfTagname).ToList();
                                if (lstUpdate.Count > 0)
                                {
                                    for (int i = 0; i < lstUpdate.Count; i++)
                                    {
                                        lstUpdate[i].pagLink = curPage[0].pagTagName;
                                        tbPageDB.tbPage_Update(lstUpdate[i]);
                                    }
                                }
                                #endregion
                                db.SubmitChanges();

                                pnlErr.Visible = true;
                                ltrErr.Text = "Cập nhật bài viết thành công !";
                                Resetcontrol();
                                BindData();
                                hidID.Value = "";

                                pnlListForder.Visible = true;
                                pnlAddForder.Visible = false;
                            }
                        }
                        #endregion
                    }
                }
            }
        }

        #region[Resetcontrol]
        void Resetcontrol()
        {
            txtTieudetin.Text = "";
            txtImage.Text = "";
            txtTieudetitle.Text = "";
            txtDesscription.Text = "";
            txtKeyword.Text = "";
            hidID.Value = "";
            ddlForder.SelectedValue = "0";
        }
        #endregion

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberOrder = (TextBox)sender;
            var lblID = (Label)txtNumberOrder.FindControl("lblID");
            var _lib = db.Libraries.FirstOrDefault(s => s.Id == int.Parse(lblID.Text));
            if (_lib != null)
            {
                _lib.Ord = int.Parse(txtNumberOrder.Text);
                db.SubmitChanges();
            }

            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";

        }
    }
}