﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="comment.ascx.cs" Inherits="MyWeb.control.panel.website.comment" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý bình luận</li>
    </ol>
    <!-- end breadcrumb -->

    <asp:UpdatePanel ID="udpComment" runat="server" RenderMode="Block" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Panel ID="pnlListForder" runat="server" Visible="true">
                <h1 class="page-header">Quản lý bình luận</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                                <h4 class="panel-title">Danh sách bình luận</h4>
                            </div>

                            <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                                <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="drlTrangthai" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlTrangthai_SelectedIndexChanged" class="form-control input-sm">
                                            <asp:ListItem Value="0">Chọn chuyên mục</asp:ListItem>
                                            <asp:ListItem Value="1">Tin tức</asp:ListItem>
                                            <asp:ListItem Value="2">Sản phẩm</asp:ListItem>
                                            <asp:ListItem Value="3">Tour</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlNhom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlNhom_SelectedIndexChanged" class="form-control input-sm" Visible="false"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="drltp" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drltp_SelectedIndexChanged" class="form-control input-sm" Visible="false"></asp:DropDownList>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                                <thead>
                                                    <tr>
                                                        <th width="10">
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                        </th>
                                                        <th>Họ tên</th>
                                                        <th width="180">Email</th>
                                                        <th width="180">Thuộc tin/sản phẩm</th>
                                                        <th width="120">Ngày đăng</th>
                                                        <th width="80">Trạng thái</th>
                                                        <th width="20"></th>
                                                        <th width="100">Công cụ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr class="even gradeC">
                                                                <td>
                                                                    <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                                    <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"comId")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <%#DataBinder.Eval(Container.DataItem, "comName")%>
                                                                </td>
                                                                <td>
                                                                    <%#DataBinder.Eval(Container.DataItem, "comEmail")%>
                                                                </td>
                                                                <td>
                                                                    <%#BindCate(DataBinder.Eval(Container.DataItem, "comId").ToString())%>
                                                                </td>
                                                                <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "comDate").ToString())%></td>
                                                                <td><%#Actives(DataBinder.Eval(Container.DataItem, "comActive").ToString())%></td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"comId")%>' CommandName="Active" class="btn btn-primary btn-xs" ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "comActive").ToString())%></asp:LinkButton>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"comId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                                    <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"comId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="row dataTables_wrapper">
                                    <div class="col-sm-5">
                                        <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                            <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                        </div>
                                    </div>

                                    <div class="col-sm-7">
                                        <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                            <ul class="pagination">
                                                <li id="data-table_previous" class="paginate_button previous disabled">
                                                    <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                                </li>
                                                <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                        <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li id="data-table_next" class="paginate_button next">
                                                    <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>
                    </div>

                </div>
            </asp:Panel>

            <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
                <h1 class="page-header">Xem/sửa bình luận</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                </div>
                                <h4 class="panel-title">Xem/sửa bình luận</h4>
                            </div>

                            <div class="panel-body panel-form">
                                <div class="form-horizontal form-bordered">

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tên tin/sản phẩm:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtten" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Họ tên:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txthoten" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Địa chỉ:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtdiachi" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Email:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtemails" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ngày đăng:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtngay" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nội dung:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTomtat" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2"></label>
                                        <div class="col-md-7">
                                            <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
