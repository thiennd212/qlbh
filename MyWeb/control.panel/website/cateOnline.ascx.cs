﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class cateOnline : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            List<tbGroupOnlineDATA> _listForder = new List<tbGroupOnlineDATA>();
            _listForder = tbGroupOnlineDB.tbGroupOnline_GetByAll();

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }

        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberOrder = (TextBox)sender;
            Label strid = (Label)txtNumberOrder.FindControl("lblID");
            List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByID(strid.Text);
            if (list.Count > 0)
            {

                list[0].gOrd = txtNumberOrder.Text;
                tbGroupOnlineDB.tbGroupOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        tbGroupOnlineDB.tbGroupOnline_Delete(hidID.Value);
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công nhóm tư vấn !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    tbGroupOnlineDB.tbGroupOnline_Delete(strID);
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    break;
                case "Edit":
                    List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByID(strID);
                    hidID.Value = strID;
                    txtTen.Text = list[0].gName;
                    txtThuTu.Text = list[0].gOrd;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            Label strid = (Label)txtNameForder.FindControl("lblID");
            List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByID(strid.Text);
            if (list.Count > 0)
            {

                list[0].gName = txtNameForder.Text;
                tbGroupOnlineDB.tbGroupOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên thành công !";
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtTen.Text == "") { ltrErr2.Text = "Chưa nhập tên nhóm !"; pnlErr2.Visible = true; txtTen.Focus(); return; }

            string strID = hidID.Value;
            if (strID.Length == 0)
            {
                tbGroupOnlineDATA ObjtbGroupOnlineDATA = new tbGroupOnlineDATA();
                ObjtbGroupOnlineDATA.gName = txtTen.Text;
                ObjtbGroupOnlineDATA.gOrd = txtThuTu.Text;
                if (tbGroupOnlineDB.tbGroupOnline_Add(ObjtbGroupOnlineDATA))
                {
                    pnlErr.Visible = true;
                    ltrErr.Text = "Thêm mới thành công !";
                    BindData();
                    Resetcontrol();
                    pnlListForder.Visible = true;
                    pnlAddForder.Visible = false;
                }
            }
            else
            {
                List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByID(strID);
                if (list.Count > 0)
                {
                    list[0].ID = strID;
                    list[0].gName = txtTen.Text;
                    list[0].gOrd = txtThuTu.Text;
                    if (tbGroupOnlineDB.tbGroupOnline_Update(list[0]))
                    {
                        pnlErr.Visible = true;
                        ltrErr.Text = "Cập nhật thành công !";
                        Resetcontrol();
                        BindData();
                        hidID.Value = "";
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                }
            }
        }

        void Resetcontrol()
        {
            txtTen.Text = "";
            hidID.Value = "";
            txtThuTu.Text = "1";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }
    }
}