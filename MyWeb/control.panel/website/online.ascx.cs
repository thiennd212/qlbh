﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class online : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }
        }

        void BindForder()
        {
            List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByAll();
            drlKieu.Items.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                drlKieu.Items.Add(new ListItem(list[i].gName, list[i].ID));
            }
        }

        void BindData()
        {
            List<tbOnlineDATA> _listForder = new List<tbOnlineDATA>();
            _listForder = tbOnlineDB.tbOnline_GetByLang(lang);
            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        tbOnlineDB.tbOnline_Delete(hidID.Value);
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công tư vấn onile!";
            BindData();
            chkSelectAll.Checked = false;
        }

        void Resetcontrol()
        {
            hidID.Value = "";
            txtNhanvien.Text = "";
            txtDienthoai.Text = "";
            txtNickZalo.Text = "";
            txtNickSkype.Text = "";
            txtEmail.Text = "";
            txtFace.Text = "";
            txtZalo.Text = "";
            txtThuTu.Text = "1";
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            var lblID = (Label)txtNameForder.FindControl("lblID");
            List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].onName1 = txtNameForder.Text;
                tbOnlineDB.tbOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên thành công !";
        }

        protected void txtTell_TextChanged(object sender, EventArgs e)
        {
            TextBox txtTell = (TextBox)sender;
            var lblID = (Label)txtTell.FindControl("lblID");
            List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].onTell = txtTell.Text;
                tbOnlineDB.tbOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật điện thoại thành công !";
        }

        protected void txtONnick_TextChanged(object sender, EventArgs e)
        {
            TextBox txtONnick = (TextBox)sender;
            var lblID = (Label)txtONnick.FindControl("lblID");
            List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].onYahoo = txtONnick.Text;
                tbOnlineDB.tbOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật nick yahoo thành công !";
        }

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberOrder = (TextBox)sender;
            Label lblID = (Label)txtNumberOrder.FindControl("lblID");
            List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].onOrd = txtNumberOrder.Text;
                tbOnlineDB.tbOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        protected string BindCate(string intID)
        {
            string str = "";
            List<tbGroupOnlineDATA> list = tbGroupOnlineDB.tbGroupOnline_GetByID(intID);
            if (list.Count > 0)
                str = list[0].gName;
            return str;
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    tbOnlineDB.tbOnline_Delete(strID);
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    break;
                case "Edit":
                    List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(strID);
                    hidID.Value = strID;
                    try
                    {
                        ddlType.SelectedValue = list[0].onName;
                    }
                    catch { }
                    txtNhanvien.Text = list[0].onName1;
                    txtDienthoai.Text = list[0].onTell;
                    txtNickZalo.Text = list[0].onYahoo;
                    txtNickSkype.Text = list[0].onSkype;
                    txtEmail.Text = list[0].onEmail;
                    txtFace.Text = list[0].onFace;
                    txtZalo.Text = list[0].onZalo;
                    txtThuTu.Text = list[0].onOrd;
                    drlKieu.SelectedValue = list[0].onType;
                    if (list[0].onActive == "1")
                    {
                        chkKichhoat.Checked = true;
                    }
                    else
                    {
                        chkKichhoat.Checked = false;
                    }
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Active":
                    List<tbOnlineDATA> lst = tbOnlineDB.tbOnline_GetByID(strID);
                    if (lst.Count > 0)
                    {
                        if (lst[0].onActive == "0")
                        {
                            lst[0].onActive = "1";
                        }
                        else { lst[0].onActive = "0"; }
                        tbOnlineDB.tbOnline_Update(lst[0]);
                    }
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
            }
        }

        protected bool Validation()
        {
            if (txtNhanvien.Text == "") { ltrErr2.Text = "Chưa nhập tên thành viên !"; pnlErr2.Visible = true; return false; }
            if (txtDienthoai.Text == "") { ltrErr2.Text = "Chưa nhập số điện thoại  !"; pnlErr2.Visible = true; return false; }
            if (txtNickZalo.Text == "") { ltrErr2.Text = "Chưa nhập tên nick!"; pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {

                if (drlKieu.SelectedValue == "0")
                {
                    pnlErr2.Visible = true;
                    ltrErr2.Text = "Chưa chọn kiểu tư vấn !!"; return;
                }
                
                string sonActive = "";
                if (chkKichhoat.Checked == true)
                {
                    sonActive = "1";
                }
                else
                {
                    sonActive = "0";
                }
                string strID = hidID.Value;
                if (strID.Length == 0)
                {
                    tbOnlineDATA ObjtbOnlineDATA = new tbOnlineDATA();
                    ObjtbOnlineDATA.onName = ddlType.SelectedValue;
                    ObjtbOnlineDATA.onName1 = txtNhanvien.Text;
                    ObjtbOnlineDATA.onNick = "";
                    ObjtbOnlineDATA.onTell = txtDienthoai.Text;
                    ObjtbOnlineDATA.onYahoo = txtNickZalo.Text;
                    ObjtbOnlineDATA.onSkype = txtNickSkype.Text;
                    ObjtbOnlineDATA.onEmail = txtEmail.Text;
                    ObjtbOnlineDATA.onFace = txtFace.Text;
                    ObjtbOnlineDATA.onZalo= txtZalo.Text;
                    ObjtbOnlineDATA.onActive = sonActive;
                    ObjtbOnlineDATA.onOrd = txtThuTu.Text;
                    ObjtbOnlineDATA.onType = drlKieu.SelectedValue;
                    ObjtbOnlineDATA.onLang = lang;
                    if (tbOnlineDB.tbOnline_Add(ObjtbOnlineDATA))
                    {
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới thành công !";
                        BindData();
                        Resetcontrol();
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                }
                else
                {
                    List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(strID);
                    if (list.Count > 0)
                    {
                        list[0].onId = strID;
                        list[0].onName = ddlType.SelectedValue;
                        list[0].onName1 = txtNhanvien.Text;
                        list[0].onTell = txtDienthoai.Text;
                        list[0].onNick = "";
                        list[0].onYahoo = txtNickZalo.Text;
                        list[0].onSkype = txtNickSkype.Text;
                        list[0].onEmail = txtEmail.Text;
                        list[0].onFace = txtFace.Text;
                        list[0].onZalo = txtZalo.Text;
                        list[0].onActive = sonActive;
                        list[0].onOrd = txtThuTu.Text;
                        list[0].onType = drlKieu.SelectedValue;
                        list[0].onLang = lang;
                        if (tbOnlineDB.tbOnline_Update(list[0]))
                        {
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật thành công !";
                            Resetcontrol();
                            BindData();
                            hidID.Value = "";
                            pnlListForder.Visible = true;
                            pnlAddForder.Visible = false;
                        }
                    }
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        protected void txtSkype_TextChanged(object sender, EventArgs e)
        {
            TextBox txtSkype = (TextBox)sender;
            var lblID = (Label)txtSkype.FindControl("lblID");
            List<tbOnlineDATA> list = tbOnlineDB.tbOnline_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].onSkype = txtSkype.Text;
                tbOnlineDB.tbOnline_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật nick skype thành công !";
        }
    }
}