﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Configuration;
using System.Globalization;

namespace MyWeb
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class webService : System.Web.Services.WebService
    {
        private string lang = "vi";
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public List<entProductSearch> getProductList()
        {
            dataAccessDataContext db = new dataAccessDataContext();
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.pageProducts == false)
            {
                var _ds = db.tbNews.Where(s => s.newActive == 0 && s.newLang == lang).OrderByDescending(s => s.newCreateDate).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();

                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].newid.ToString());
                        item.strName = _ds[i].newName;

                        if (_ds[i].newImage != null && _ds[i].newImage.Length > 0)
                        {
                            item.strImg = _ds[i].newImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);

                    }
                }

                return arrProductSearch;
            }
            else
            {
                var _ds = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang).OrderByDescending(s => s.proId).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();
                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].proId.ToString());
                        item.strName = _ds[i].proName;

                        if (_ds[i].proImage != null && _ds[i].proImage.Length > 0)
                        {
                            item.strImg = _ds[i].proImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);
                    }
                }
                return arrProductSearch;
            }

        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public bool SendOrder(string proid, string soluong, string hoten, string email, string phone, string phoneCD, string hinhthuc, string diachi, string content, string attr)
        {
            try
            {
                dataAccessDataContext db = new dataAccessDataContext();
                string madonhang = "";
                var pro = db.tbProducts.FirstOrDefault(x => x.proId == int.Parse(proid));
                string tensp = pro.proName;
                string masp = pro.proCode;
                string price = (!string.IsNullOrEmpty(pro.proPrice)) ? pro.proPrice : pro.proOriginalPrice;
                string money = (int.Parse(price) * int.Parse(soluong)).ToString();                

                #region Tao don hang
                tbCustomersDATA ObjtbCustomersDATA = new tbCustomersDATA();
                ObjtbCustomersDATA.Acount = "";
                if (Session["uidr"] != null)
                {
                    ObjtbCustomersDATA.Acount = Session["uidr"].ToString();
                }
                ObjtbCustomersDATA.Pas = "";
                ObjtbCustomersDATA.name = hoten;
                ObjtbCustomersDATA.mail = email;
                ObjtbCustomersDATA.Address = diachi;
                ObjtbCustomersDATA.phone = phone;
                ObjtbCustomersDATA.detail = content;
                ObjtbCustomersDATA.totalmoney = money;
                ObjtbCustomersDATA.lang = lang;
                ObjtbCustomersDATA.Tell = phoneCD;
                ObjtbCustomersDATA.Payment = ShowPay(hinhthuc);
                tbCustomersDB.tbCustomers_Add(ObjtbCustomersDATA);

                #region Add mã đơn hàng
                tbCustomer updateOrderCode = db.tbCustomers.OrderByDescending(x => x.id).FirstOrDefault();
                updateOrderCode.OrderCode = "MDH" + DateTime.Now.ToString("ddMMyy",
                                    CultureInfo.InvariantCulture) + "" + updateOrderCode.id;
                madonhang = updateOrderCode.OrderCode;
                updateOrderCode.CreateDateCus = DateTime.Now;
                db.SubmitChanges();
                #endregion

                List<tbCustomersDATA> listmax = tbCustomersDB.tbCustomers_GetByMaxID();
                tbShopingcartDATA ObjtbShopingcartDATA = new tbShopingcartDATA();
                ObjtbShopingcartDATA.proid = proid;
                ObjtbShopingcartDATA.cus_id = listmax[0].id;
                ObjtbShopingcartDATA.proname = tensp;
                ObjtbShopingcartDATA.price = price;
                ObjtbShopingcartDATA.number = soluong;
                ObjtbShopingcartDATA.money = money;
                ObjtbShopingcartDATA.createdate = DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                ObjtbShopingcartDATA.lang = lang;
                tbShopingcartDB.tbShopingcart_Add(ObjtbShopingcartDATA);
                #endregion

                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;

                string host = ConfigurationManager.AppSettings["url-web-base"];
                string hostName = host.Replace("http://", "").Replace("/", "").Trim();

                string cc = "";
                string Noidung = "";
                Noidung += "<table>";
                Noidung += "<tr><td colspan =\"2\"><h3>THÔNG TIN ĐƠN HÀNG</h3></td></tr>";
                Noidung += "<tr><td colspan =\"2\">Cảm ơn bạn đã mua hàng tại: <b><a href=\"" + host + "\" target=\"_blank\">" + hostName + "</a></b></tr>";
                Noidung += "<tr><td colspan =\"2\">Thông tin của bạn:</tr>";
                Noidung += "<tr><td><b>Mã đơn hàng:</b></td><td>" + madonhang + "</td></tr>";
                Noidung += hoten != "" ? "<tr><td><b>Họ tên:</b></td><td>" + hoten + "</td></tr>" : "";
                Noidung += email != "" ? "<tr><td><b>Email:</b></td><td>" + email + "</td></tr>" : "";
                Noidung += phone != "" ? "<tr><td><b>Điện thoại di động:</b></td><td>" + phone + "</td></tr>" : "";
                Noidung += phoneCD != "" ? "<tr><td><b>Điện thoại cố định:</b></td><td>" + phoneCD + "</td></tr>" : "";
                Noidung += hinhthuc != "" ? "<tr><td><b>Hình thức thanh toán:</b></td><td>" + ShowPay(hinhthuc) + "</td></tr>" : "";
                Noidung += diachi != "" ? "<tr><td><b>Địa chỉ:</b></td><td>" + diachi + "</td></tr>" : "";
                Noidung += content != "" ? "<tr><td><b>Yêu cầu thêm:</b></td><td>" + content + "</td></tr>" : "";
                Noidung += tensp != "" ? "<tr><td><b>Tên sản phẩm:</b></td><td>" + tensp + "</td></tr>" : "";
                Noidung += masp != "" ? "<tr><td><b>Mã sản phẩm:</b></td><td>" + masp + "</td></tr>" : "";
                Noidung += attr != "" ? "<tr><td><b>Thuộc tính sản phẩm:</b></td><td>" + attr + "</td></tr>" : "";
                Noidung += "</table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin đặt hàng từ " + hoten, Noidung);
                    common.SendMail(email.Trim(), "", "", "", "Thông tin đơn hàng từ website " + hostName + "", Noidung);
                }
                catch
                {
                    return false;
                }
                #endregion

                return true;
            }
            catch
            {
                return false;
            }
        }
        public static String ShowPay(string strName)
        {
            if (strName == "1") return MyWeb.Global.GetLangKey("payment_method_1");
            else if (strName == "2") return MyWeb.Global.GetLangKey("payment_method_2");
            else if (strName == "3") return MyWeb.Global.GetLangKey("payment_method_3");
            else if (strName == "4") return MyWeb.Global.GetLangKey("payment_method_4");
            else
                return "";
        }


        [System.Web.Services.WebMethod(EnableSession = true)]
        public bool SendRegister(string hoTen, string email, string phone, string content, string duan)
        {
            try
            {
                dataAccessDataContext db = new dataAccessDataContext();
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;
                string pas = list[0].conMail_Pass;
                string host = host = "smtp.gmail.com";
                if (list[0].conMail_Method.Length > 0)
                {
                    host = list[0].conMail_Method;
                }
                int post = 465;
                if (list[0].conMail_Port.Length > 0)
                {
                    post = int.Parse(list[0].conMail_Port);
                }

                string tenduan = "";
                if (duan != "" && duan != "0")
                {
                    var chkDuAn = db.tbProjects.FirstOrDefault(x => x.proId.ToString() == duan);
                    if (chkDuAn != null)
                        tenduan = chkDuAn.proName;
                }

                string cc = "";
                string Noidung = "";
                Noidung += "<table>";
                Noidung += "<tr><td colspan =\"2\"><h3>THÔNG TIN ĐĂNG KÝ NHẬN TƯ VẤN MIỄN PHÍ</h3></td></tr>";
                Noidung += "<tr><td colspan =\"2\">Cảm ơn bạn đã đăng ký nhận tư vấn miễn phí tại: <b><a href=\"http://sankinhdo.vn/\" target=\"_blank\">sankinhdo.vn</a></b></tr>";
                Noidung += "<tr><td colspan =\"2\">Thông tin của bạn:</tr>";
                Noidung += hoTen != "" ? "<tr><td><b>Thông tin khách hàng:</b></td><td>" + hoTen + "</td></tr>" : "";
                Noidung += phone != "" ? "<tr><td><b>Điện thoại:</b></td><td>" + phone + "</td></tr>" : "";
                Noidung += email != "" ? "<tr><td><b>Email:</b></td><td>" + email + "</td></tr>" : "";
                Noidung += content != "" ? "<tr><td><b>Yêu cầu thêm:</b></td><td>" + content + "</td></tr>" : "";
                Noidung += tenduan != "" ? "<tr><td><b>Dự án:</b></td><td>" + tenduan + "</td></tr>" : "";
                Noidung += "</table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + hoTen, Noidung);
                    common.SendMail(email.Trim(), "", "", "", "Thông tin liên hệ trên website sankinhdo.vn", Noidung);
                }
                catch
                {
                    return false;
                }
                #endregion
                string file_path = "";
                tbContact ObjtbContact = new tbContact();
                ObjtbContact.conName = common.killChars(hoTen);
                ObjtbContact.conCompany = "";
                ObjtbContact.conAddress = "";
                ObjtbContact.conTel = common.killChars(phone);
                ObjtbContact.conMail = common.killChars(email);
                ObjtbContact.conDetail = common.killChars(content);
                ObjtbContact.conActive = 0;
                ObjtbContact.conLang = lang;
                ObjtbContact.conPositions = "";
                ObjtbContact.confax = "";
                ObjtbContact.confile = file_path;
                ObjtbContact.conwebsite = "";
                ObjtbContact.conType = 10;
                ObjtbContact.conDate = DateTime.Now;
                ObjtbContact.conProjId = Convert.ToInt32(duan);

                db.tbContacts.InsertOnSubmit(ObjtbContact);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public bool SendProContact(string hoten, string dongxe, string hinhthuc, string diachi, string email, string phone)
        {
            try
            {
                dataAccessDataContext db = new dataAccessDataContext();
                string host = HttpContext.Current.Request.Url.Host;
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailto = list[0].conMail_Info;

                string cc = "";
                string Noidung = "";
                Noidung += "<table>";
                Noidung += "<tr><td colspan =\"2\"><h3>THÔNG TIN ĐĂNG KÝ NHẬN TƯ VẤN XE MIỄN PHÍ</h3></td></tr>";
                Noidung += "<tr><td colspan =\"2\">Cảm ơn bạn đã đăng ký nhận tư vấn xe miễn phí tại: <b><a href=\"http://" + host + "\" target=\"_blank\">" + host + "</a></b></tr>";
                Noidung += "<tr><td colspan =\"2\">Thông tin của bạn:</tr>";
                Noidung += hoten != "" ? "<tr><td><b>Thông tin khách hàng:</b></td><td>" + hoten + "</td></tr>" : "";
                Noidung += dongxe != "" ? "<tr><td><b>Dòng xe:</b></td><td>" + dongxe + "</td></tr>" : "";
                Noidung += hinhthuc != "" ? "<tr><td><b>Hình thức:</b></td><td>" + hinhthuc + "</td></tr>" : "";
                Noidung += diachi != "" ? "<tr><td><b>Địa chỉ:</b></td><td>" + diachi + "</td></tr>" : "";
                Noidung += email != "" ? "<tr><td><b>Email:</b></td><td>" + email + "</td></tr>" : "";
                Noidung += phone != "" ? "<tr><td><b>Số điện thoại:</b></td><td>" + phone + "</td></tr>" : "";
                Noidung += "</table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + hoten, Noidung);
                    common.SendMail(email.Trim(), "", "", "", "Thông tin liên hệ trên website " + host + "", Noidung);
                }
                catch
                {
                    return false;
                }
                #endregion
                string file_path = "";
                tbContact ObjtbContact = new tbContact();
                ObjtbContact.conName = common.killChars(hoten);
                ObjtbContact.conCompany = "";
                ObjtbContact.conAddress = common.killChars(diachi);
                ObjtbContact.conTel = common.killChars(phone);
                ObjtbContact.conMail = common.killChars(email);
                ObjtbContact.conActive = 0;
                ObjtbContact.conLang = lang;
                ObjtbContact.conPositions = "";
                ObjtbContact.confax = "";
                ObjtbContact.confile = file_path;
                ObjtbContact.conwebsite = "";
                ObjtbContact.conType = 11;
                ObjtbContact.conDate = DateTime.Now;
                ObjtbContact.conProType = hinhthuc;
                ObjtbContact.conProCatName = dongxe;

                db.tbContacts.InsertOnSubmit(ObjtbContact);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public bool AddProjectTag(int prjId, string title, string order, string content)
        {
            try
            {
                dataAccessDataContext db = new dataAccessDataContext();

                db.tbProjectTags.DeleteAllOnSubmit(db.tbProjectTags.Where(x => x.proId == prjId));

                tbProjectTag prjTag = new tbProjectTag();
                prjTag.proId = prjId;
                prjTag.TagName = title;
                prjTag.Ord = Convert.ToInt32(order);
                prjTag.TagValue = content;
                prjTag.Active = 1;
                db.tbProjectTags.InsertOnSubmit(prjTag);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public class entProductSearch
        {
            private int _id;
            public int id
            {
                get { return _id; }
                set { _id = value; }
            }

            private string _strName;
            public string strName
            {
                get { return _strName; }
                set { _strName = value; }
            }

            private string _strImg;
            public string strImg
            {
                get { return _strImg; }
                set { _strImg = value; }
            }
        }
    }
}
