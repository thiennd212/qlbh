﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbGroupOnlineDB
{
    #region[tbGroupOnline_Add]
    public static bool tbGroupOnline_Add(tbGroupOnlineDATA _tbGroupOnlineDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@gName", _tbGroupOnlineDATA.gName));
                dbCmd.Parameters.Add(new SqlParameter("@gOrd", _tbGroupOnlineDATA.gOrd));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbGroupOnline_Update]
    public static bool tbGroupOnline_Update(tbGroupOnlineDATA _tbGroupOnlineDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", _tbGroupOnlineDATA.ID));
                dbCmd.Parameters.Add(new SqlParameter("@gName", _tbGroupOnlineDATA.gName));
                dbCmd.Parameters.Add(new SqlParameter("@gOrd", _tbGroupOnlineDATA.gOrd));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbGroupOnline_Delete]
    public static bool tbGroupOnline_Delete(string ID)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@ID", ID));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbGroupOnline_GetByAll]
    public static List<tbGroupOnlineDATA> tbGroupOnline_GetByAll()
    {
        List<tbGroupOnlineDATA> list = new List<tbGroupOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbGroupOnlineDATA objtbGroupOnlineDATA = new tbGroupOnlineDATA(
                        reader["ID"].ToString(),
                        reader["gName"].ToString(),
                        reader["gOrd"].ToString());
                        list.Add(objtbGroupOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbGroupOnline_GetByID]
    public static List<tbGroupOnlineDATA> tbGroupOnline_GetByID(string sID)
    {
        List<tbGroupOnlineDATA> list = new List<tbGroupOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", sID));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbGroupOnlineDATA objtbGroupOnlineDATA = new tbGroupOnlineDATA(
                        reader["ID"].ToString(),
                        reader["gName"].ToString(),
                        reader["gOrd"].ToString());
                        list.Add(objtbGroupOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbGroupOnline_GetByInOnline]
    public static List<tbGroupOnlineDATA> tbGroupOnline_GetByInOnline()
    {
        List<tbGroupOnlineDATA> list = new List<tbGroupOnlineDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbGroupOnline_GetByInOnline", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbGroupOnlineDATA objtbGroupOnlineDATA = new tbGroupOnlineDATA(
                        reader["ID"].ToString(),
                        reader["gName"].ToString(),
                        reader["gOrd"].ToString());
                        list.Add(objtbGroupOnlineDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}