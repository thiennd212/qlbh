﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

public class EncryptCode
{
    public static string Encrypt(string value)
    {
        if (string.IsNullOrEmpty(value))
            return string.Empty;
        var md5 = new MD5CryptoServiceProvider();
        byte[] valueArray = Encoding.ASCII.GetBytes(value);
        valueArray = md5.ComputeHash(valueArray);
        var sb = new StringBuilder();
        for (int i = 0; i < valueArray.Length; i++)
            sb.Append(valueArray[i].ToString("x2").ToLower());
        return sb.ToString();
    }
    public static string Encode(string str)
    {
        byte[] encbuff = Encoding.UTF8.GetBytes(str);
        string strtemp = Convert.ToBase64String(encbuff);
        string strtam = "";
        Int32 i = 0, len = strtemp.Length;
        for (i = 2; i <= len; i += 2)
        {
            strtam = strtam + strtemp.Substring(i - 2, 2) + RandomString(1);
        }
        strtam = strtam + strtemp.Substring(i - 2, len - (i - 2));
        return strtam;
    }
    #region Random String
    public static string RandomString(int size)
    {
        Random rnd = new Random();
        string srds = "";
        string[] str = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
        for (int i = 0; i < size; i++)
        {
            srds = srds + str[rnd.Next(0, 61)];
        }
        return srds;
    }
    #endregion
    #region Name To Tag
    public static string NameToTag(string strName)
    {
        string strReturn = "";
        Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        strReturn = Regex.Replace(strName, "[^\\w\\s]", string.Empty).Replace(" ", "-").ToLower();
        string strFormD = strReturn.Normalize(System.Text.NormalizationForm.FormD);
        return regex.Replace(strFormD, string.Empty).Replace("đ", "d");
    }
    #endregion
}