﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbShopingcartDATA
{
    #region[Declare variables]
    private string _proid;
    private string _cus_id;
    private string _proname;
    private string _price;
    private string _number;
    private string _money;
    private string _createdate;
    private string _lang;
    #endregion
    #region[Function]
    public tbShopingcartDATA() { }
    public tbShopingcartDATA(string proid_, string cus_id_, string proname_, string price_, string number_, string money_, string createdate_, string lang_)
    {
        _proid = proid_;
        _cus_id = cus_id_;
        _proname = proname_;
        _price = price_;
        _number = number_;
        _money = money_;
        _createdate = createdate_;
        _lang = lang_;
    }
    #endregion
    #region[Assigned value]
    public string proid { get { return _proid; } set { _proid = value; } }
    public string cus_id { get { return _cus_id; } set { _cus_id = value; } }
    public string proname { get { return _proname; } set { _proname = value; } }
    public string price { get { return _price; } set { _price = value; } }
    public string number { get { return _number; } set { _number = value; } }
    public string money { get { return _money; } set { _money = value; } }
    public string createdate { get { return _createdate; } set { _createdate = value; } }
    public string lang { get { return _lang; } set { _lang = value; } }
    #endregion
}