﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbCustomersDB
{
    #region[tbCustomers_Add]
    public static bool tbCustomers_Add(tbCustomersDATA _tbCustomersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Acount", _tbCustomersDATA.Acount));
                dbCmd.Parameters.Add(new SqlParameter("@Pas", _tbCustomersDATA.Pas));
                dbCmd.Parameters.Add(new SqlParameter("@name", _tbCustomersDATA.name));
                dbCmd.Parameters.Add(new SqlParameter("@mail", _tbCustomersDATA.mail));
                dbCmd.Parameters.Add(new SqlParameter("@Address", _tbCustomersDATA.Address));
                dbCmd.Parameters.Add(new SqlParameter("@phone", _tbCustomersDATA.phone));
                dbCmd.Parameters.Add(new SqlParameter("@detail", _tbCustomersDATA.detail));
                dbCmd.Parameters.Add(new SqlParameter("@totalmoney", _tbCustomersDATA.totalmoney));
                dbCmd.Parameters.Add(new SqlParameter("@lang", _tbCustomersDATA.lang));
                dbCmd.Parameters.Add(new SqlParameter("@Tell", _tbCustomersDATA.Tell));
                dbCmd.Parameters.Add(new SqlParameter("@Payment", _tbCustomersDATA.Payment));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbCustomers_Update]
    public static bool tbCustomers_Update(tbCustomersDATA _tbCustomersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@id", _tbCustomersDATA.id));
                dbCmd.Parameters.Add(new SqlParameter("@Acount", _tbCustomersDATA.Acount));
                dbCmd.Parameters.Add(new SqlParameter("@Pas", _tbCustomersDATA.Pas));
                dbCmd.Parameters.Add(new SqlParameter("@name", _tbCustomersDATA.name));
                dbCmd.Parameters.Add(new SqlParameter("@mail", _tbCustomersDATA.mail));
                dbCmd.Parameters.Add(new SqlParameter("@Address", _tbCustomersDATA.Address));
                dbCmd.Parameters.Add(new SqlParameter("@phone", _tbCustomersDATA.phone));
                dbCmd.Parameters.Add(new SqlParameter("@detail", _tbCustomersDATA.detail));
                dbCmd.Parameters.Add(new SqlParameter("@totalmoney", _tbCustomersDATA.totalmoney));
                dbCmd.Parameters.Add(new SqlParameter("@lang", _tbCustomersDATA.lang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbCustomers_Delete]
    public static bool tbCustomers_Delete(string sid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@id", sid));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbCustomers_GetByAll]
    public static List<tbCustomersDATA> tbCustomers_GetByAll()
    {
        List<tbCustomersDATA> list = new List<tbCustomersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbCustomersDATA objtbCustomersDATA = new tbCustomersDATA(
                        reader["id"].ToString(),
                        reader["Acount"].ToString(),
                        reader["Pas"].ToString(),
                        reader["name"].ToString(),
                        reader["mail"].ToString(),
                        reader["Address"].ToString(),
                        reader["phone"].ToString(),
                        reader["detail"].ToString(),
                        reader["totalmoney"].ToString(),
                        reader["lang"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Payment"].ToString(),
                        reader["status"].ToString());
                        list.Add(objtbCustomersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbCustomersDATA> tbCustomers_WhereByLinq(string where)
    {
        List<tbCustomersDATA> list = new List<tbCustomersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("tbCustomers_WhereByLinq", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@where", where));
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbCustomersDATA obj = new tbCustomersDATA();
                        obj.id = reader["id"].ToString();
                        obj.Acount = reader["Acount"].ToString();
                        obj.name = reader["name"].ToString();
                        obj.mail = reader["mail"].ToString();
                        obj.Address = reader["Address"].ToString();
                        //obj.proname=reader["proname"].ToString();
                        obj.phone = reader["phone"].ToString();
                        obj.detail = reader["detail"].ToString();
                        obj.totalmoney = reader["totalmoney"].ToString();
                        obj.lang = reader["lang"].ToString();
                        obj.Tell = reader["Tell"].ToString();
                        obj.Payment = reader["Payment"].ToString();
                        obj.status = reader["status"].ToString();
                        //obj.proid = reader["proid"].ToString();
                        //obj.cus_id = reader["cus_id"].ToString();
                        //obj.proname = reader["proname"].ToString();
                        //obj.price = reader["price"].ToString();
                        //obj.number = reader["number"].ToString();
                        //obj.money = reader["money"].ToString();
                        obj.createdate = reader["createdate"].ToString();
                        //obj.Groupname = reader["groupname"].ToString();
                        list.Add(obj);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbCustomers_GetByID]
    public static List<tbCustomersDATA> tbCustomers_GetByID(string sid)
    {
        List<tbCustomersDATA> list = new List<tbCustomersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@id", sid));
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbCustomersDATA objtbCustomersDATA = new tbCustomersDATA(
                        reader["id"].ToString(),
                        reader["Acount"].ToString(),
                        reader["Pas"].ToString(),
                        reader["name"].ToString(),
                        reader["mail"].ToString(),
                        reader["Address"].ToString(),
                        reader["phone"].ToString(),
                        reader["detail"].ToString(),
                        reader["totalmoney"].ToString(),
                        reader["lang"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Payment"].ToString(),
                        reader["status"].ToString());
                        list.Add(objtbCustomersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbCustomers_GetByMaxID]
    public static List<tbCustomersDATA> tbCustomers_GetByMaxID()
    {
        List<tbCustomersDATA> list = new List<tbCustomersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_GetByMaxID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbCustomersDATA objtbCustomersDATA = new tbCustomersDATA(
                        reader["id"].ToString(),
                        reader["Acount"].ToString(),
                        reader["Pas"].ToString(),
                        reader["name"].ToString(),
                        reader["mail"].ToString(),
                        reader["Address"].ToString(),
                        reader["phone"].ToString(),
                        reader["detail"].ToString(),
                        reader["totalmoney"].ToString(),
                        reader["lang"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Payment"].ToString(),
                        reader["status"].ToString());
                        list.Add(objtbCustomersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion

    #region[tbCustomers_Status]
    public static bool tbCustomers_UpdateStatus(tbCustomersDATA _tbCustomersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbCustomers_UpdateStatus", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@id", _tbCustomersDATA.id));
                dbCmd.Parameters.Add(new SqlParameter("@status", _tbCustomersDATA.status));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
}