﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbContactDB
{
    #region[tbContact_Add]
    public static bool tbContact_Add(tbContactDATA _tbContactDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conName", _tbContactDATA.conName));
                dbCmd.Parameters.Add(new SqlParameter("@conCompany", _tbContactDATA.conCompany));
                dbCmd.Parameters.Add(new SqlParameter("@conAddress", _tbContactDATA.conAddress));
                dbCmd.Parameters.Add(new SqlParameter("@conTel", _tbContactDATA.conTel));
                dbCmd.Parameters.Add(new SqlParameter("@conMail", _tbContactDATA.conMail));
                dbCmd.Parameters.Add(new SqlParameter("@conDetail", _tbContactDATA.conDetail));
                dbCmd.Parameters.Add(new SqlParameter("@conActive", _tbContactDATA.conActive));
                dbCmd.Parameters.Add(new SqlParameter("@conLang", _tbContactDATA.conLang));
                dbCmd.Parameters.Add(new SqlParameter("@conPositions", _tbContactDATA.conPositions));
                dbCmd.Parameters.Add(new SqlParameter("@confax", _tbContactDATA.confax));
                dbCmd.Parameters.Add(new SqlParameter("@confile", _tbContactDATA.confile));
                dbCmd.Parameters.Add(new SqlParameter("@conwebsite", _tbContactDATA.conwebsite));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbContact_Update]
    public static bool tbContact_Update(tbContactDATA _tbContactDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conId", _tbContactDATA.conId));
                dbCmd.Parameters.Add(new SqlParameter("@conCompany", _tbContactDATA.conCompany));
                dbCmd.Parameters.Add(new SqlParameter("@conAddress", _tbContactDATA.conAddress));
                dbCmd.Parameters.Add(new SqlParameter("@conTel", _tbContactDATA.conTel));
                dbCmd.Parameters.Add(new SqlParameter("@conMail", _tbContactDATA.conMail));
                dbCmd.Parameters.Add(new SqlParameter("@conDetail", _tbContactDATA.conDetail));
                dbCmd.Parameters.Add(new SqlParameter("@conDate", _tbContactDATA.conDate));
                dbCmd.Parameters.Add(new SqlParameter("@conActive", _tbContactDATA.conActive));
                dbCmd.Parameters.Add(new SqlParameter("@conLang", _tbContactDATA.conLang));
                dbCmd.Parameters.Add(new SqlParameter("@conPositions", _tbContactDATA.conPositions));
                dbCmd.Parameters.Add(new SqlParameter("@confax", _tbContactDATA.confax));
                dbCmd.Parameters.Add(new SqlParameter("@confile", _tbContactDATA.confile));
                dbCmd.Parameters.Add(new SqlParameter("@conwebsite", _tbContactDATA.conwebsite));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbContact_UpdateconActive]
    public static bool tbContact_UpdateconActive(tbContactDATA _tbContactDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_UpdateconActive", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conId", _tbContactDATA.conId));
                dbCmd.Parameters.Add(new SqlParameter("@conActive", _tbContactDATA.conActive));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbContact_Delete]
    public static bool tbContact_Delete(string sconId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@conId", sconId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbContact_GetByAll]
    public static List<tbContactDATA> tbContact_GetByAll(string slinLang)
    {
        List<tbContactDATA> list = new List<tbContactDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@linLang", slinLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbContactDATA objtbContactDATA = new tbContactDATA(
                        reader["conId"].ToString(),
                        reader["conName"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conMail"].ToString(),
                        reader["conDetail"].ToString(),
                        reader["conDate"].ToString(),
                        reader["conActive"].ToString(),
                        reader["conLang"].ToString(),
                        reader["conPositions"].ToString(),
                        reader["confax"].ToString(),
                        reader["confile"].ToString(),
                        reader["conwebsite"].ToString());
                        list.Add(objtbContactDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbContact_GetByID]
    public static List<tbContactDATA> tbContact_GetByID(string sconId)
    {
        List<tbContactDATA> list = new List<tbContactDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conId", sconId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbContactDATA objtbContactDATA = new tbContactDATA(
                        reader["conId"].ToString(),
                        reader["conName"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conMail"].ToString(),
                        reader["conDetail"].ToString(),
                        reader["conDate"].ToString(),
                        reader["conActive"].ToString(),
                        reader["conLang"].ToString(),
                        reader["conPositions"].ToString(),
                        reader["confax"].ToString(),
                        reader["confile"].ToString(),
                        reader["conwebsite"].ToString());
                        list.Add(objtbContactDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbContact_GetByChuoi]
    public static List<tbContactDATA> tbContact_GetByChuoi(string Chuoi)
    {
        List<tbContactDATA> list = new List<tbContactDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbContact where 1=1 " + Chuoi + "ORDER BY conDate DESC", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbContactDATA objtbContactDATA = new tbContactDATA(
                        reader["conId"].ToString(),
                        reader["conName"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conMail"].ToString(),
                        reader["conDetail"].ToString(),
                        reader["conDate"].ToString(),
                        reader["conActive"].ToString(),
                        reader["conLang"].ToString(),
                        reader["conPositions"].ToString(),
                        reader["confax"].ToString(),
                        reader["confile"].ToString(),
                        reader["conwebsite"].ToString());
                        list.Add(objtbContactDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbContacts_Count]
    public static string tbContacts_Count(string strQuery)
    {
        string str = "0";
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand(strQuery, dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                try
                {
                    str = dbCmd.ExecuteScalar().ToString();
                }
                catch { }
                dbConn.Close();
            }
        }
        return str;
    }
    #endregion

    #region[tbContact_Paging]
    public static List<tbContactDATA> tbContact_Paging(string CurentPage, string PageSize, string Fields, string Filter, string Sort)
    {
        List<tbContactDATA> list = new List<tbContactDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbContact_Paging", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@CurentPage", CurentPage));
                dbCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));
                dbCmd.Parameters.Add(new SqlParameter("@Fields", Fields));
                dbCmd.Parameters.Add(new SqlParameter("@Filter", Filter));
                dbCmd.Parameters.Add(new SqlParameter("@Sort", Sort));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbContactDATA objtbNewsDATA = new tbContactDATA(
                        reader["conId"].ToString(),
                        reader["conName"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conMail"].ToString(),
                        reader["conDetail"].ToString(),
                        reader["conDate"].ToString(),
                        reader["conActive"].ToString(),
                        reader["conLang"].ToString(),
                        reader["conPositions"].ToString(),
                        reader["confax"].ToString(),
                        reader["confile"].ToString(),
                        reader["conwebsite"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}