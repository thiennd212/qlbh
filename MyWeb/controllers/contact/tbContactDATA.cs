﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbContactDATA
{
    #region[Declare variables]
    private string _conId;
    private string _conName;
    private string _conCompany;
    private string _conAddress;
    private string _conTel;
    private string _conMail;
    private string _conDetail;
    private string _conDate;
    private string _conActive;
    private string _conLang;
    private string _conPositions;
    private string _confax;
    private string _confile;
    private string _conwebsite;
    #endregion
    #region[Function]
    public tbContactDATA() { }
    public tbContactDATA(string conId_, string conName_, string conCompany_, string conAddress_, string conTel_,
        string conMail_, string conDetail_, string conDate_, string conActive_, string conLang_,
        string conPositions_, string confax_, string confile_, string conwebsite_)
    {
        _conId = conId_;
        _conName = conName_;
        _conCompany = conCompany_;
        _conAddress = conAddress_;
        _conTel = conTel_;
        _conMail = conMail_;
        _conDetail = conDetail_;
        _conDate = conDate_;
        _conActive = conActive_;
        _conLang = conLang_;
        _conPositions = conPositions_;
        _confax = confax_;
        _confile = confile_;
        _conwebsite = conwebsite_;
    }
    #endregion
    #region[Assigned value]
    public string conId { get { return _conId; } set { _conId = value; } }
    public string conName { get { return _conName; } set { _conName = value; } }
    public string conCompany { get { return _conCompany; } set { _conCompany = value; } }
    public string conAddress { get { return _conAddress; } set { _conAddress = value; } }
    public string conTel { get { return _conTel; } set { _conTel = value; } }
    public string conMail { get { return _conMail; } set { _conMail = value; } }
    public string conDetail { get { return _conDetail; } set { _conDetail = value; } }
    public string conDate { get { return _conDate; } set { _conDate = value; } }
    public string conActive { get { return _conActive; } set { _conActive = value; } }
    public string conLang { get { return _conLang; } set { _conLang = value; } }
    public string conPositions { get { return _conPositions; } set { _conPositions = value; } }
    public string confax { get { return _confax; } set { _confax = value; } }
    public string confile { get { return _confile; } set { _confile = value; } }
    public string conwebsite { get { return _conwebsite; } set { _conwebsite = value; } }
    #endregion
}