﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbConfigDATA
{
    #region[Declare variables]
    private string _conid;
    private string _conCompany;
    private string _conAddress;
    private string _conTel;
    private string _conFax;
    private string _conWebsite;
    private string _conMail_Method;
    private string _conMail_Smtp;
    private string _conMail_Port;
    private string _conMail_Info;
    private string _conMail_Noreply;
    private string _conMail_Pass;
    private string _conNews_Hot;
    private string _conNews_Subject;
    private string _conNews_Next;
    private string _conEditorial;
    private string _conContact;
    private string _conContact1;
    private string _conCopyright;
    private string _catId;
    private string _conTitle;
    private string _conKeywords;
    private string _conDescription;
    private string _conLang;
    private string _SEO;
    private string _showSEO;
    private string _viewNews;
    private string _viewProducts;
    private string _viewBanner;
    private string _viewSocial;
    private string _viewPayment;
    private string _sortBy;
    private string _viewBy;
    private string _pageError;
    private string _Field1;
    private string _Field2;
    private string _Field3;
    private string _Field4;
    private string _Field5;
    private string _livechat;
    private string _conPageRedirect;
    private string _typePageRedirect;
    #endregion
    #region[Function]
    public tbConfigDATA() { }
    public tbConfigDATA(string conid_, string conCompany_, string conAddress_, string conTel_, string conFax_,
        string conWebsite_, string conMail_Method_, string conMail_Smtp_, string conMail_Port_, string conMail_Info_,
        string conMail_Noreply_, string conMail_Pass_, string conNews_Hot_, string conNews_Subject_, string conNews_Next_,
        string conEditorial_, string conContact_, string conContact1_, string conCopyright_, string catId_,
        string conTitle_, string conKeywords_, string conDescription_, string conLang_, string SEO_, string showSEO_, string viewNews_, string viewProducts_, string viewBanner_, string viewSocial_, string viewPayment_, string sortBy_, string viewProducts9_, string pageError_, string Field1_, string Field2_, string Field3_, string Field4_, string Field5_, string livechat_, string conPageRedirect_, string typePageRedirect_)
    {
        _conid = conid_;
        _conCompany = conCompany_;
        _conAddress = conAddress_;
        _conTel = conTel_;
        _conFax = conFax_;
        _conWebsite = conWebsite_;
        _conMail_Method = conMail_Method_;
        _conMail_Smtp = conMail_Smtp_;
        _conMail_Port = conMail_Port_;
        _conMail_Info = conMail_Info_;
        _conMail_Noreply = conMail_Noreply_;
        _conMail_Pass = conMail_Pass_;
        _conNews_Hot = conNews_Hot_;
        _conNews_Subject = conNews_Subject_;
        _conNews_Next = conNews_Next_;
        _conEditorial = conEditorial_;
        _conContact = conContact_;
        _conContact1 = conContact1_;
        _conCopyright = conCopyright_;
        _catId = catId_;
        _conTitle = conTitle_;
        _conKeywords = conKeywords_;
        _conDescription = conDescription_;
        _conLang = conLang_;
        _SEO = SEO_;
        _showSEO = showSEO_;
        _viewNews = viewNews_;
        _viewProducts = viewProducts_;
        _viewBanner = viewBanner_;
        _viewSocial = viewSocial_;
        _viewPayment = viewPayment_;
        _sortBy = sortBy_;
        _viewBy = viewProducts9_;
        _pageError = pageError_;
        _Field1 = Field1_;
        _Field2 = Field2_;
        _Field3 = Field3_;
        _Field4 = Field4_;
        _Field5 = Field5_;
        _livechat = livechat_;
        _conPageRedirect = conPageRedirect_;
        _typePageRedirect = typePageRedirect_;
    }
    #endregion
    #region[Assigned value]
    public string livechat { get { return _livechat; } set { _livechat = value; } }
    public string conid { get { return _conid; } set { _conid = value; } }
    public string conCompany { get { return _conCompany; } set { _conCompany = value; } }
    public string conAddress { get { return _conAddress; } set { _conAddress = value; } }
    public string conTel { get { return _conTel; } set { _conTel = value; } }
    public string conFax { get { return _conFax; } set { _conFax = value; } }
    public string conWebsite { get { return _conWebsite; } set { _conWebsite = value; } }
    public string conMail_Method { get { return _conMail_Method; } set { _conMail_Method = value; } }
    public string conMail_Smtp { get { return _conMail_Smtp; } set { _conMail_Smtp = value; } }
    public string conMail_Port { get { return _conMail_Port; } set { _conMail_Port = value; } }
    public string conMail_Info { get { return _conMail_Info; } set { _conMail_Info = value; } }
    public string conMail_Noreply { get { return _conMail_Noreply; } set { _conMail_Noreply = value; } }
    public string conMail_Pass { get { return _conMail_Pass; } set { _conMail_Pass = value; } }
    public string conNews_Hot { get { return _conNews_Hot; } set { _conNews_Hot = value; } }
    public string conNews_Subject { get { return _conNews_Subject; } set { _conNews_Subject = value; } }
    public string conNews_Next { get { return _conNews_Next; } set { _conNews_Next = value; } }
    public string conEditorial { get { return _conEditorial; } set { _conEditorial = value; } }
    public string conContact { get { return _conContact; } set { _conContact = value; } }
    public string conContact1 { get { return _conContact1; } set { _conContact1 = value; } }
    public string conCopyright { get { return _conCopyright; } set { _conCopyright = value; } }
    public string catId { get { return _catId; } set { _catId = value; } }
    public string conTitle { get { return _conTitle; } set { _conTitle = value; } }
    public string conKeywords { get { return _conKeywords; } set { _conKeywords = value; } }
    public string conDescription { get { return _conDescription; } set { _conDescription = value; } }
    public string conLang { get { return _conLang; } set { _conLang = value; } }
    public string SEO { get { return _SEO; } set { _SEO = value; } }
    public string showSEO { get { return _showSEO; } set { _showSEO = value; } }
    public string viewNews { get { return _viewNews; } set { _viewNews = value; } }
    public string viewProducts { get { return _viewProducts; } set { _viewProducts = value; } }
    public string viewBanner { get { return _viewBanner; } set { _viewBanner = value; } }
    public string viewSocial { get { return _viewSocial; } set { _viewSocial = value; } }
    public string viewPayment { get { return _viewPayment; } set { _viewPayment = value; } }
    public string sortBy { get { return _sortBy; } set { _sortBy = value; } }
    public string viewBy { get { return _viewBy; } set { _viewBy = value; } }
    public string pageError { get { return _pageError; } set { _pageError = value; } }
    public string Field1 { get { return _Field1; } set { _Field1 = value; } }
    public string Field2 { get { return _Field2; } set { _Field2 = value; } }
    public string Field3 { get { return _Field3; } set { _Field3 = value; } }
    public string Field4 { get { return _Field4; } set { _Field4 = value; } }
    public string Field5 { get { return _Field5; } set { _Field5 = value; } }
    public string ConPageRedirect
    {
        get { return _conPageRedirect; }
        set { _conPageRedirect = value; }
    }
    public string TypePageRedirect
    {
        get { return _typePageRedirect; }
        set { _typePageRedirect = value; }
    }
    #endregion
}