﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbConfigDB
{
    #region[tbConfig_Add]
    public static bool tbConfig_Add(tbConfigDATA _tbConfigDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conCompany", _tbConfigDATA.conCompany));
                dbCmd.Parameters.Add(new SqlParameter("@conAddress", _tbConfigDATA.conAddress));
                dbCmd.Parameters.Add(new SqlParameter("@conTel", _tbConfigDATA.conTel));
                dbCmd.Parameters.Add(new SqlParameter("@conFax", _tbConfigDATA.conFax));
                dbCmd.Parameters.Add(new SqlParameter("@conWebsite", _tbConfigDATA.conWebsite));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Method", _tbConfigDATA.conMail_Method));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Smtp", _tbConfigDATA.conMail_Smtp));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Port", _tbConfigDATA.conMail_Port));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Info", _tbConfigDATA.conMail_Info));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Noreply", _tbConfigDATA.conMail_Noreply));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Pass", _tbConfigDATA.conMail_Pass));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Hot", _tbConfigDATA.conNews_Hot));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Subject", _tbConfigDATA.conNews_Subject));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Next", _tbConfigDATA.conNews_Next));
                dbCmd.Parameters.Add(new SqlParameter("@conEditorial", _tbConfigDATA.conEditorial));
                dbCmd.Parameters.Add(new SqlParameter("@conContact", _tbConfigDATA.conContact));
                dbCmd.Parameters.Add(new SqlParameter("@conContact1", _tbConfigDATA.conContact1));
                dbCmd.Parameters.Add(new SqlParameter("@conCopyright", _tbConfigDATA.conCopyright));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbConfigDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@conTitle", _tbConfigDATA.conTitle));
                dbCmd.Parameters.Add(new SqlParameter("@conKeywords", _tbConfigDATA.conKeywords));
                dbCmd.Parameters.Add(new SqlParameter("@conDescription", _tbConfigDATA.conDescription));
                dbCmd.Parameters.Add(new SqlParameter("@conLang", _tbConfigDATA.conLang));
                dbCmd.Parameters.Add(new SqlParameter("@SEO", _tbConfigDATA.SEO));
                dbCmd.Parameters.Add(new SqlParameter("@showSEO", _tbConfigDATA.showSEO));
                dbCmd.Parameters.Add(new SqlParameter("@viewNews", _tbConfigDATA.viewNews));
                dbCmd.Parameters.Add(new SqlParameter("@viewProducts", _tbConfigDATA.viewProducts));
                dbCmd.Parameters.Add(new SqlParameter("@viewBanner", _tbConfigDATA.viewBanner));
                dbCmd.Parameters.Add(new SqlParameter("@viewSocial", _tbConfigDATA.viewSocial));
                dbCmd.Parameters.Add(new SqlParameter("@viewPayment", _tbConfigDATA.viewPayment));
                dbCmd.Parameters.Add(new SqlParameter("@livechat", _tbConfigDATA.livechat));
                dbCmd.Parameters.Add(new SqlParameter("@conPageRedirect", _tbConfigDATA.ConPageRedirect));
                dbCmd.Parameters.Add(new SqlParameter("@typePageRedirect", _tbConfigDATA.TypePageRedirect));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbConfig_Update]
    public static bool tbConfig_Update(tbConfigDATA _tbConfigDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conid", _tbConfigDATA.conid));
                dbCmd.Parameters.Add(new SqlParameter("@conCompany", _tbConfigDATA.conCompany));
                dbCmd.Parameters.Add(new SqlParameter("@conAddress", _tbConfigDATA.conAddress));
                dbCmd.Parameters.Add(new SqlParameter("@conTel", _tbConfigDATA.conTel));
                dbCmd.Parameters.Add(new SqlParameter("@conFax", _tbConfigDATA.conFax));
                dbCmd.Parameters.Add(new SqlParameter("@conWebsite", _tbConfigDATA.conWebsite));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Method", _tbConfigDATA.conMail_Method));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Smtp", _tbConfigDATA.conMail_Smtp));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Port", _tbConfigDATA.conMail_Port));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Info", _tbConfigDATA.conMail_Info));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Noreply", _tbConfigDATA.conMail_Noreply));
                dbCmd.Parameters.Add(new SqlParameter("@conMail_Pass", _tbConfigDATA.conMail_Pass));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Hot", _tbConfigDATA.conNews_Hot));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Subject", _tbConfigDATA.conNews_Subject));
                dbCmd.Parameters.Add(new SqlParameter("@conNews_Next", _tbConfigDATA.conNews_Next));
                dbCmd.Parameters.Add(new SqlParameter("@conEditorial", _tbConfigDATA.conEditorial));
                dbCmd.Parameters.Add(new SqlParameter("@conContact", _tbConfigDATA.conContact));
                dbCmd.Parameters.Add(new SqlParameter("@conContact1", _tbConfigDATA.conContact1));
                dbCmd.Parameters.Add(new SqlParameter("@conCopyright", _tbConfigDATA.conCopyright));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbConfigDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@conTitle", _tbConfigDATA.conTitle));
                dbCmd.Parameters.Add(new SqlParameter("@conKeywords", _tbConfigDATA.conKeywords));
                dbCmd.Parameters.Add(new SqlParameter("@conDescription", _tbConfigDATA.conDescription));
                dbCmd.Parameters.Add(new SqlParameter("@conLang", _tbConfigDATA.conLang));
                dbCmd.Parameters.Add(new SqlParameter("@SEO", _tbConfigDATA.SEO));
                dbCmd.Parameters.Add(new SqlParameter("@showSEO", _tbConfigDATA.showSEO));
                dbCmd.Parameters.Add(new SqlParameter("@viewNews", _tbConfigDATA.viewNews));
                dbCmd.Parameters.Add(new SqlParameter("@viewProducts", _tbConfigDATA.viewProducts));
                dbCmd.Parameters.Add(new SqlParameter("@viewBanner", _tbConfigDATA.viewBanner));
                dbCmd.Parameters.Add(new SqlParameter("@viewSocial", _tbConfigDATA.viewSocial));
                dbCmd.Parameters.Add(new SqlParameter("@viewPayment", _tbConfigDATA.viewPayment));
                dbCmd.Parameters.Add(new SqlParameter("@viewBy", _tbConfigDATA.viewBy));
                dbCmd.Parameters.Add(new SqlParameter("@sortBy", _tbConfigDATA.sortBy));
                dbCmd.Parameters.Add(new SqlParameter("@pageError", _tbConfigDATA.pageError));
                dbCmd.Parameters.Add(new SqlParameter("@livechat", _tbConfigDATA.livechat));
                dbCmd.Parameters.Add(new SqlParameter("@conPageRedirect", _tbConfigDATA.ConPageRedirect));
                dbCmd.Parameters.Add(new SqlParameter("@typePageRedirect", _tbConfigDATA.TypePageRedirect));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbConfig_Delete]
    public static bool tbConfig_Delete(string sconid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@conid", sconid));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbConfig_GetID]
    public static List<tbConfigDATA> tbConfig_GetID(string sconid)
    {
        List<tbConfigDATA> list = new List<tbConfigDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_GetID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conid", sconid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbConfigDATA objtbConfigDATA = new tbConfigDATA(
                        reader["conid"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conFax"].ToString(),
                        reader["conWebsite"].ToString(),
                        reader["conMail_Method"].ToString(),
                        reader["conMail_Smtp"].ToString(),
                        reader["conMail_Port"].ToString(),
                        reader["conMail_Info"].ToString(),
                        reader["conMail_Noreply"].ToString(),
                        reader["conMail_Pass"].ToString(),
                        reader["conNews_Hot"].ToString(),
                        reader["conNews_Subject"].ToString(),
                        reader["conNews_Next"].ToString(),
                        reader["conEditorial"].ToString(),
                        reader["conContact"].ToString(),
                        reader["conContact1"].ToString(),
                        reader["conCopyright"].ToString(),
                        reader["catId"].ToString(),
                        reader["conTitle"].ToString(),
                        reader["conKeywords"].ToString(),
                        reader["conDescription"].ToString(),
                        reader["conLang"].ToString(),
                        reader["SEO"].ToString(),
                        reader["showSEO"].ToString(),
                        reader["viewNews"].ToString(),
                        reader["viewProducts"].ToString(),
                        reader["viewBanner"].ToString(),
                        reader["viewSocial"].ToString(),
                        reader["viewPayment"].ToString(),
                        reader["viewBy"].ToString(),
                        reader["sortBy"].ToString(),
                        reader["pageError"].ToString(),
                        reader["Field1"].ToString(),
                        reader["Field2"].ToString(),
                        reader["Field3"].ToString(),
                        reader["Field4"].ToString(),
                        reader["Field5"].ToString(), reader["livechat"].ToString(), reader["conPageRedirect"].ToString(), reader["typePageRedirect"].ToString());
                        list.Add(objtbConfigDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbConfig_GetLang]
    public static List<tbConfigDATA> tbConfig_GetLang(string sconLang)
    {
        List<tbConfigDATA> list = new List<tbConfigDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_GetLang", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conLang", sconLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbConfigDATA objtbConfigDATA = new tbConfigDATA(
                        reader["conid"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conFax"].ToString(),
                        reader["conWebsite"].ToString(),
                        reader["conMail_Method"].ToString(),
                        reader["conMail_Smtp"].ToString(),
                        reader["conMail_Port"].ToString(),
                        reader["conMail_Info"].ToString(),
                        reader["conMail_Noreply"].ToString(),
                        reader["conMail_Pass"].ToString(),
                        reader["conNews_Hot"].ToString(),
                        reader["conNews_Subject"].ToString(),
                        reader["conNews_Next"].ToString(),
                        reader["conEditorial"].ToString(),
                        reader["conContact"].ToString(),
                        reader["conContact1"].ToString(),
                        reader["conCopyright"].ToString(),
                        reader["catId"].ToString(),
                        reader["conTitle"].ToString(),
                        reader["conKeywords"].ToString(),
                        reader["conDescription"].ToString(),
                        reader["conLang"].ToString(),
                        reader["SEO"].ToString(),
                        reader["showSEO"].ToString(),
                        reader["viewNews"].ToString(),
                        reader["viewProducts"].ToString(),
                        reader["viewBanner"].ToString(),
                        reader["viewSocial"].ToString(),
                        reader["viewPayment"].ToString(),
                        reader["viewBy"].ToString(),
                        reader["sortBy"].ToString(),
                        reader["pageError"].ToString(),
                        reader["Field1"].ToString(),
                        reader["Field2"].ToString(),
                        reader["Field3"].ToString(),
                        reader["Field4"].ToString(),
                        reader["Field5"].ToString(), reader["livechat"].ToString(), reader["conPageRedirect"].ToString(), reader["typePageRedirect"].ToString());

                        list.Add(objtbConfigDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbConfig_GetLangCatId]
    public static List<tbConfigDATA> tbConfig_GetLangCatId(string sconLang, string scatId)
    {
        List<tbConfigDATA> list = new List<tbConfigDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbConfig_GetLangCatId", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@conLang", sconLang));
                dbCmd.Parameters.Add(new SqlParameter("@catId", scatId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbConfigDATA objtbConfigDATA = new tbConfigDATA(
                        reader["conid"].ToString(),
                        reader["conCompany"].ToString(),
                        reader["conAddress"].ToString(),
                        reader["conTel"].ToString(),
                        reader["conFax"].ToString(),
                        reader["conWebsite"].ToString(),
                        reader["conMail_Method"].ToString(),
                        reader["conMail_Smtp"].ToString(),
                        reader["conMail_Port"].ToString(),
                        reader["conMail_Info"].ToString(),
                        reader["conMail_Noreply"].ToString(),
                        reader["conMail_Pass"].ToString(),
                        reader["conNews_Hot"].ToString(),
                        reader["conNews_Subject"].ToString(),
                        reader["conNews_Next"].ToString(),
                        reader["conEditorial"].ToString(),
                        reader["conContact"].ToString(),
                        reader["conContact1"].ToString(),
                        reader["conCopyright"].ToString(),
                        reader["catId"].ToString(),
                        reader["conTitle"].ToString(),
                        reader["conKeywords"].ToString(),
                        reader["conDescription"].ToString(),
                        reader["conLang"].ToString(),
                        reader["SEO"].ToString(),
                        reader["showSEO"].ToString(),
                        reader["viewNews"].ToString(),
                        reader["viewProducts"].ToString(),
                        reader["viewBanner"].ToString(),
                        reader["viewSocial"].ToString(),
                        reader["viewPayment"].ToString(),
                        reader["viewBy"].ToString(),
                        reader["sortBy"].ToString(),
                        reader["pageError"].ToString(),
                        reader["Field1"].ToString(),
                        reader["Field2"].ToString(),
                        reader["Field3"].ToString(),
                        reader["Field4"].ToString(),
                        reader["Field5"].ToString(), reader["livechat"].ToString(), reader["conPageRedirect"].ToString(), reader["typePageRedirect"].ToString());
                        list.Add(objtbConfigDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}