﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="detailProject.ascx.cs" Inherits="MyWeb.views.project.detailProject" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<div id="projDetail">
    <div class="projInfo">
        <asp:HiddenField ID="hdProjId" runat="server" />
        <asp:Literal runat="server" ID="ltrInfo"></asp:Literal>
    </div>
    <div class="projContent">
        <asp:Literal ID="ltrTabs" runat="server"></asp:Literal>
    </div>    
    <div class="projOther">
        <asp:Literal runat="server" ID="ltrOther"></asp:Literal>
    </div>
</div>
<!-- Modal -->
<!--End modal-->
<script type="text/javascript">
    $(document).ready(function () {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            navigation: true,
            navigationText: ["«", "»"],
            rewindNav: false,
            scrollPerPage: false,
            slideSpeed: 1500,
            pagination: false,
            paginationNumbers: false,
            autoPlay: false,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        var r = {
            'special': /[\W]/g,
            'quotes': /[^0-9^]/g,
            'notnumbers': /[^a-zA]/g
        }
        function valid(o, w) {
            o.value = o.value.replace(r[w], '');
        }

        function syncPosition(el) {
            var current = this.currentItem;
            $("#sync2")
              .find(".owl-item")
              .removeClass("synced")
              .eq(current)
              .addClass("synced")
            if ($("#sync2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#sync2").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
</script>

<script type="text/javascript">
    CloudZoom.quickStart();
    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });
</script>
