﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.control.panel.system;

namespace MyWeb.views.project
{
    public partial class ucProjectSale : System.Web.UI.UserControl
    {
        private dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadList();
            }
        }

        protected void LoadList()
        {
            var listProject =
                db.tbProjects.Where(s => s.proActive == 1 && s.proLang == lang && s.proBanchay == 1).OrderByDescending(s => s.proDate).Take(3).ToList();
            string strResult = "";

            if (listProject.Any())
            {
                int j = 1;
                string host = "http://" + HttpContext.Current.Request.Url.Host;
                foreach (var item in listProject)
                {
                    string urlShare = host + "/" + item.proTagName + ".html";
                    string class4 = "projItems";
                    if (j % 4 == 0)
                        class4 = "projItems projItems4";
                    string imgUrl = (!string.IsNullOrEmpty(item.proImage)) ? item.proImage.Split(Convert.ToChar(","))[0] : "";
                    strResult += "<div class='" + class4 + "'>";
                    strResult += "  <div class='projItems_info'>";
                    strResult += "      <a class='projItems_img' href='/" + item.proTagName + ".html' title='" + item.proName + "'>";
                    strResult += "          <span class='projItems_ticket_sale'>Sale</span>";
                    strResult += "          <img src='" + imgUrl + "' alt='" + item.proName + "' />";
                    strResult += "          <span class='projItems_priceOrifin'> $" + common.FormatNumber(item.proPrice) + "/m2</span>";
                    strResult += "      </a>";
                    strResult += "      <a class='projItems_link' href='/" + item.proTagName + ".html' title='" + item.proName + "'>" + item.proName + "</a>";
                    strResult += "      <div class='projItems_add'>" + item.proWarranty + "</div>";
                    strResult += "      <div class=\"projItems_des\">" + common.NganXau(item.proContent, 200) + " <a href='/" + item.proTagName + ".html' title=\"Xem thêm\">Xem thêm</a></div>";
                    strResult += "      <div class\"projItems_date\"><i class=\"fa fa-calendar pull-left\" aria-hidden=\"true\"></i><span class=\"pull-left left-date\">" + common.ConvertDate(Convert.ToDateTime(item.proDate)) + "</span><span class\"pull-right right-share\"><i class=\"fa fa-share-alt pull-right\" aria-hidden=\"true\" id=\"share-button\" onclick=\"ShareFB('" + urlShare + "')\"></i></span></div>";
                    strResult += "  </div>";
                    strResult += "</div>";
                    j++;
                }
            }

            ltrListItem.Text = strResult;
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s=>s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}