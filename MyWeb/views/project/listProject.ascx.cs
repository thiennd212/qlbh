﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.control.panel.system;

namespace MyWeb.views.project
{
    public partial class listProject : System.Web.UI.UserControl
    {
        dataAccessDataContext db =new dataAccessDataContext();
        public string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            var listCat = new List<tbPage>();
            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";
            int pageSize = 18;

            if (Request["hp"] != null)
            {
                var parentCat =
                    db.tbPages.FirstOrDefault(
                        s => s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.GPJ) && s.pagTagName == Request["hp"].ToString());
                if (parentCat != null)
                {
                    listCat =
                        db.tbPages.Where(
                            s =>
                                s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.GPJ) &&
                                s.paglevel.StartsWith(parentCat.paglevel)).ToList();

                    ltrHeader.Text = "<div class='listProjectHeader'><a title='" + parentCat.pagName + "' href='/" +
                                 parentCat.pagTagName + ".html'>" + parentCat.pagName + "</a></div>";

                    var arrId = listCat.Select(s => s.pagId).ToArray();
                    var listProject =
                        db.tbProjects.Where(s => s.proLang == lang && s.proActive == 1 && arrId.Contains(Convert.ToInt32(s.catId))).OrderByDescending(x => x.proDate).ToList();

                    if (listProject.Any())
                    {
                        int count = listProject.Count;

                        listProject = listProject.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                        int j = 1;
                        foreach (var item in listProject)
                        {
                            string class3 = "projItems";
                            if (j % 4 == 0)
                                class3 = "projItems projItems3";
                            string imgUrl = (!string.IsNullOrEmpty(item.proImage))?item.proImage.Split(Convert.ToChar(","))[0]:"";
                            strResult += "<div class='" + class3 + "'>";
                            strResult += "  <div class='projItems_info'>";
                            strResult += "      <a class='projItems_img' href='/" + item.proTagName + ".html' title='" + item.proName + "'>";
                            strResult += "          <img src='" + imgUrl + "' alt='" + item.proName + "' />";
                            strResult += "          <span class='projItems_priceOrifin'> $" + common.FormatNumber(item.proPrice) + "/m2</span>";
                            strResult += "      </a>";
                            strResult += "      <a class='projItems_link' href='/" + item.proTagName + ".html' title='" + item.proName + "'>" + item.proName + "</a>";                                                       
                            strResult += "      <div class='projItems_des'>" + item.proWarranty + "</div>";
                            strResult += "  </div>";
                            strResult += "</div>";
                            j++;
                        }

                        ltrContent.Text = strResult;
                        ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
                    }
                }                
            }            
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }
    }
}