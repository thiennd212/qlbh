﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProNew : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = false;
        public bool showPage = false;
        string strKey = "";
        string viewBy = GlobalClass.viewProducts9;
        int pageSize = 10;
        int currentPage = 1;
        int recordCount = 0;
        string strPage = GlobalClass.viewProducts5;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (Request["key"] != null && !Request["key"].Equals(""))
            {
                strKey = Request["key"];
            }
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            }
            catch { }
            try { pageSize = int.Parse(GlobalClass.viewProducts8); }
            catch { }



            if (strPage == "1")
            {
                showPage = true;
            }
            else if (strPage == "0")
            {
                showPage = false;
            }

            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            showAdv = true;
            if (strKey != "")
            {
                IEnumerable<tbProduct> objProduct = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang);
                if (strKey == "moi")
                {
                    ltrName.Text = "<a href='/san-pham-moi.html'>" + MyWeb.Global.GetLangKey("new_products") + "</a>";
                    objProduct = objProduct.Where(s => s.proIndex == 1).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
                }
                else if (strKey == "banchay")
                {
                    ltrName.Text = "<a href='/san-pham-ban-chay.html'>" + MyWeb.Global.GetLangKey("pro_best_sale") + "</a>";
                    objProduct = objProduct.Where(s => s.proBanchay == 1).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
                }
                else if (strKey == "noibat")
                {
                    ltrName.Text = "<a href='/san-pham-noi-bat.html'>" + MyWeb.Global.GetLangKey("pro_sl_nb") + "</a>";
                    objProduct = objProduct.Where(s => s.proNoibat == 1).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
                }
                else if (strKey == "khuyenmai")
                {
                    ltrName.Text = "<a href='/san-pham-khuyen-mai.html'>" + MyWeb.Global.GetLangKey("pro_sl_km") + "</a>";
                    objProduct = objProduct.Where(s => s.proKM == 1).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
                }

                recordCount = objProduct.Count();
                objProduct = objProduct.Skip((currentPage - 1) * pageSize).Take(pageSize);
                viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;


                ltrProduct.Text = common.LoadProductList(objProduct, viewBy);
                ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
                ltrPaging.Visible = true;

            }
            else
            {
                ltrName.Text = MyWeb.Global.GetLangKey("new_products");
                IEnumerable<tbProduct> objProduct = db.tbProducts.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).Where(s => s.proActive == 1 && s.proLang.Trim() == lang && s.proIndex == 1);

                recordCount = objProduct.Count();
                objProduct = objProduct.Skip((currentPage - 1) * pageSize).Take(pageSize);

                viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;
                ltrProduct.Text = common.LoadProductList(objProduct, viewBy);

                ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
                
                ltrPaging.Visible = true;
            }
        }
    }
}