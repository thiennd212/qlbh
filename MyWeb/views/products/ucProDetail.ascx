﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProDetail.ascx.cs" Inherits="MyWeb.views.products.ucProDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<div class="box-pro-detailt-sub">
    <div class="list-cat-child col-md-12">
            <asp:Literal ID="ltrListCatChild" runat="server"></asp:Literal>
        </div>
    <asp:HiddenField ID="hidID" runat="server" />
    <div class="pro-detail">
        <div class="detail_box_1">
            <asp:Repeater ID="rptpro" runat="server">
                <ItemTemplate>
                    <div class="form-left">
                        <div id="sync1" class="owl-carousel">
                            <%#BindImageSlide(Eval("proImage").ToString(), Eval("proName").ToString(),"1")%>
                        </div>
                        <div id="sync2" class="owl-carousel">
                            <%#BindImageSlide(Eval("proImage").ToString(), Eval("proName").ToString(),"2")%>
                        </div>
                    </div>
                    <div class="form-right form_right_0">
                        <input type="hidden" id="TenSanPham" value="<%#DataBinder.Eval(Container.DataItem, "proName")%>" />
                        <input type="hidden" id="MaSanPham" value="<%#Eval("proCode") %>" />
                        <input type="hidden" id="ProId" value="<%#Eval("proId") %>" />
                        <input type="hidden" id="ProPrice" value="<%#Eval("proId") %>" />
                        <input type="hidden" id="ProAttr" value="" />
                        <h1 class="color-text-head"><%#DataBinder.Eval(Container.DataItem, "proName")%></h1>
                         <div class="price_all">
                            <div class="text-price">
                                Giá bán: 
                                <%#BindSalePrice(Eval("proPrice").ToString(), MyWeb.Global.GetLangKey("currency_symbol")) %> ( <%#Status(Eval("proStatus").ToString()) %> )
                            </div>
                            <div class="text-price-niem-yet">
                                Giá niêm yết: 
                                <%#BindSalePrice(Eval("proOriginalPrice").ToString(), MyWeb.Global.GetLangKey("currency_symbol")) %>
                            </div>
                        </div>
                        <div class="model-product">
                            Model: 
                            <%#GetModel(Eval("modelId"))%>
                        </div>
                        <div class="baohanh-product">
                            Bảo hành: 
                            <%#DataBinder.Eval(Container.DataItem, "baoHanh")%>
                        </div>                        
                        <div class="text-line">
                            <% = MyWeb.Global.GetLangKey("view_code") %>: <%#Eval("proCode") %>
                        </div>
                        <div class="info-status">
                            Tình trạng: <%#GetProStatus(Eval("proStatus").ToString())%>
                        </div>

                        <div class="info-manu">
                            Xuất xứ: <%#GetProManu(Eval("manufacturerId"))%>
                        </div>
                        <div class="attrs-product clearfix">
                            <%#BindAttr(Eval("proId").ToString())%>
                        </div>
                        <div class="info-sort">
                            Mô tả ngắn: <%#DataBinder.Eval(Container.DataItem, "proWarranty")%>
                        </div>
                        <div class="info-content">
                            <div class="hotline_coppy">
                                <p><span class="hotline_coppy_3">Mua online với giá rẻ hơn</span> <span class="hotline_coppy_2">Gọi ngay: </span><span class="hotline_coppy_1"></span></p>
                            </div>
                            <div class="main">Nội dung mô tả tóm tắt: <%#Eval("proContent")%></div>
                        </div>
                        <div class="info-cout">
                            Số lượng: <%#Eval("proCount")%>
                        </div>

                        <div class="info-date">
                            Ngày đăng: <%#GetProDate(Eval("proDate"))%>
                        </div>
                        <div class="info-promotion-content">
                            Nội dung khuyến mãi: <%#(Eval("proPromotions").ToString())%>
                        </div>
                        <div class="info-promotion-from">
                            Khuyến mãi từ: <%#GetProDate(Eval("proTungay"))%>
                        </div>
                        <div class="info-promotion-to">
                            Khuyến mãi đến: <%#GetProDate(Eval("proDenngay"))%>
                        </div>
                        <div class="phone_number">
                            <div class="info-hotline hotline_num1">
                                <span>Tư vấn kinh doanh:</span> <%#GetHotline()%>
                            </div>
                            <div class="info-hotline info-hotline-kythuat">
                                <span>Tư vấn kỹ thuật:</span> <%#GetHotlineKyThuat()%>
                            </div>
                        </div>
                        <div class="info-tag">
                            Tag: <%#RenderTag(Eval("proTag").ToString())%>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="form-right form_right_1">
                <div class="text-line text_line_none">
                    <div class="row">
                        <div class="col-lg-6">
                            <%=MyWeb.Global.GetLangKey("cart_amount") %><br>
                            <input type="number" class="in_none_1 input-card boder-color" value="1" id="proQuantity" min="1" max="1000" />
                        </div>
                        <div class="col-lg-6">
                            <%=MyWeb.Global.GetLangKey("cart_money") %>:<br>
                            <span class="in_none_1 price-total color-text-second"><span id="total"></span></span>
                        </div>
                    </div>
                </div>
                <div class="vt_1">
                    <div class="text-line">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col_phone_1">
                                    <!-- <asp:LinkButton ID="btnAddNow" runat="server" class="btn-add-card" OnClick="btnAddNow_Click"><%=MyWeb.Global.GetLangKey("pro_button_order1")%></asp:LinkButton> -->
                                    <button type="button" class="btn btn-info btn-lg btn-add-card" data-toggle="modal" data-target="#myModalOrder"><%=MyWeb.Global.GetLangKey("pro_button_order1")%></button>
                                </div>
                                <div class="col_phone_2">
                                    <asp:LinkButton ID="btnAdd" runat="server" class="btn-add-card order-now" OnClick="btnAdd_Click"><%=MyWeb.Global.GetLangKey("pro_button_order2")%></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="phone_all"></div>
                </div>

                <div class="cart_policy">
                    <div class="bill_accep">
                        <ul class="list_card_img">
                            <p>Chấp nhận thanh toán: </p>
                            <li><a href="javascript:void(0)">
                                <img src="uploads/layout/default/css/images/bill_accep_1.png" alt=""></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="uploads/layout/default/css/images/bill_accep_2.png" alt=""></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="uploads/layout/default/css/images/bill_accep_3.png" alt=""></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="uploads/layout/default/css/images/bill_accep_4.png" alt=""></a></li>
                            <li><a href="javascript:void(0)">
                                <img src="uploads/layout/default/css/images/bill_accep_5.png" alt=""></a></li>
                        </ul>
                        <ul class="policy">
                            <li>Ưu đãi đặc biệt: ĐỔI MỚI SP miễn phí trong 45 ngày, chi tiết tại đây</li>
                            <li>Bao giá hàng điện máy trong 7 ngày, chi tiết tại đây.</li>
                            <li>Ưu đãi trả góp Lãi suất 0% - Nhận thêm quà tặng, chi tiết tại đây.</li>
                        </ul>
                    </div>
                </div>

                <div class="div-addthis">
                    <div class="addthis_native_toolbox"></div>
                </div>
            </div>
            <%if (GlobalClass.commentFacebook.Contains("1"))
              {%><div class="shares">
                  <div class="fb-comments" data-href="<%=strUrlFace %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
              </div>
            <%} %>
            <div id="areaComment">
                <asp:Literal runat="server" ID="ltrListComment"></asp:Literal>
                <asp:Literal runat="server" ID="ltrPagingComment"></asp:Literal>
                <asp:Label runat="server" ID="lbAlter" CssClass="alterComment"></asp:Label>
                <div id="formComment">
                    <div class="rows">
                        <label>Tên:</label>
                        <asp:TextBox runat="server" ID="txtCommentName" CssClass="inputComment"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCommentName" ControlToValidate="txtCommentName" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="rows">
                        <label>Email:</label>
                        <asp:TextBox runat="server" ID="txtCommentEmail" CssClass="inputComment"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCommentEmail" ControlToValidate="txtCommentEmail" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="rows">
                        <label>Nội dung:</label>
                        <asp:TextBox runat="server" ID="txtCommentContent" CssClass="inputComment" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCommentContent" ControlToValidate="txtCommentContent" SetFocusOnError="true" ForeColor="Red" ErrorMessage="(*) Yêu cầu nhập nội dung" ValidationGroup="sendComment" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="rows">
                        <asp:LinkButton runat="server" ID="lbtSendComment" OnClick="lbtSendComment_Click" ValidationGroup="sendComment">Gửi</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>

        <div class="detail_box_2">
            <%if (ltrProListSample.Text != "")
              {%>
            <div class="box-pro-sub box-pro-same">
                <div class="cate-header sub-top">
                    <div class="txt-name-sub">Sản phẩm tương tự</div>
                </div>
                <asp:Literal ID="ltrProListSample" runat="server"></asp:Literal>
                <div class="clearfix">
                    <asp:Literal ID="ltrPagingSample" runat="server"></asp:Literal>
                </div>
            </div>
            <%} %>
            <div class="clearfix">
                <asp:Literal ID="ltrTabs" runat="server"></asp:Literal>
            </div>
            <div class="box-pro-sub">
                <div class="cate-header sub-top">
                    <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("products_other")%></div>
                </div>
                <asp:Literal ID="ltrProList" runat="server"></asp:Literal>
                <div class="clearfix">
                    <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
                </div>
            </div>
        </div>

        <%if (ltrListpro2.Text != "")
          {%>
        <div class="box-pro-sub box-pro-relate">
            <div class="cate-header sub-top">
                <div class="txt-name-sub">Sản phẩm liên quan</div>
            </div>
            <asp:Literal ID="ltrListpro2" runat="server"></asp:Literal>
        </div>
        <% } %>
    </div>
</div>

<div class="modal fade" id="myModalOrder" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="header">MỜI BẠN ĐIỀN THÔNG TIN</h2>
            </div>
            <div class="modal-body clearfix">
                <div class="box-sub-book-now bookNowForm">
                    <div class="box-body-sub bookNowContent">
                        <div class="col-md-12">
                            <div class="form-group MessengerBox">
                                <div class="col-md-12">
                                    <span id="thongbao"></span>
                                </div>
                            </div>
                            <div class="body-cart-now">
                                <div class="row">
                                    <span class="view-cate">Thông tin khách hàng</span>
                                </div>
                                <div class="row">
                                    <label class="control-label col-md-3">Họ và tên*:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtHoTen" ClientIDMode="Static" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-md-3">Email:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtEmail" ClientIDMode="Static" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="control-label col-md-3">Số di động:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="control-label col-md-3">Số cố định:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtPhoneCodinh" ClientIDMode="Static" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="control-label col-md-3">Hình thức thanh toán:</label>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlthanhtoan" ClientIDMode="Static" runat="server" class="form-control"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="control-label col-md-3">Địa chỉ:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtAddress" ClientIDMode="Static" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="control-label col-md-3">Nội dung:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtContent" ClientIDMode="Static" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group bookNowBtn">
                                <div class="col-md-6">
                                    <button type="button" id="GuiDonHang" class="btn btn-info btn-lg col-md-6" onclick="GuiThonTinDatHang()">Gửi thông tin</button>
                                    <button type="button" id="HuyBo" class="btn btn-info btn-lg col-md-6" data-dismiss="modal">Hủy bỏ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="text" id="idsoluong3" value="1" runat="server" hidden="hidden" />
<script type="text/javascript">
    $("#total")[0].innerHTML = $(".text-price").find("span").text();
    $("#proQuantity").on("change keyup", function () {
        var soluong = $('#proQuantity').val();
        if (soluong > 1000) {
            $("#proQuantity").val('1000');
            $("input[id$='idsoluong3").val('1000');
            soluong = 1000;
        }
        $("input[id$='idsoluong3").val(soluong);

        var price = $("#proQuantity").val();
        $("#ctl14_ucLoadControl_ctl00_hidID").val(price);
        var money = $(".text-price").find("span").text();
        var cur = money.substring((money.indexOf(" ") + 1));
        var quantity = $("#proQuantity").val();
        money = money.substring(0, money.indexOf(" "));
        var arr = money.split(",");
        var total = 0;
        for (var i = 0 ; i <= (arr.length - 1) ; i++) {
            money = money.replace(',', '');
        }
        money = parseFloat(money);
        total = money * quantity;
        total = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
        $("#total")[0].innerHTML = total + " " + cur;

    });

    function selectAttr(item) {
        var parent = $(item).parent();

        $(parent).find("span.content-parent-attr").each(function () {
            $(this).removeClass("attr-active");
        });

        $(item).addClass("attr-active");
        var strAttr = "";
        var i = 0;
        $("div.attrs-product div.item-attr").each(function () {
            var attName = $(this).find("span.title-parent-attr");
            var att1 = attName.html();
            if (att1) {
                if (i != 0)
                    strAttr += "; ";
                strAttr += attName.html() + " ";
            }
            var attVal = $(this).find("span.attr-active");
            var att2 = attVal.html();
            if (att2) {                
                strAttr += attVal.html();                
            }
            i++;
        });
        $("#ProAttr").val(strAttr);
    }



    function GuiThonTinDatHang() {
        $("#GuiDonHang").prop("disabled", true);
        var proid = $("#ProId").val();
        var soluong = $('#proQuantity').val();
        var hoten = $("#txtHoTen").val();
        var email = $("#txtEmail").val();
        var phone = $("#txtPhone").val();
        var phoneCoDinh = $("#txtPhoneCodinh").val();
        var hinhthuc = $("#ddlthanhtoan").val();
        var diachi = $("#txtAddress").val();
        var content = $("#txtContent").val();
        var attr = $("#ProAttr").val();

        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SendOrder",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ proid: proid, soluong: soluong, hoten: hoten, email: email, phone: phone, phoneCD: phoneCoDinh, hinhthuc: hinhthuc, diachi: diachi, content: content, attr: attr }),
            success: function (response) {
                console.log(response.d);
                if (response.d == true) {
                    $("#txtHoTen").val("");
                    $("#txtEmail").val("");
                    $("#txtPhone").val("");
                    $("#txtPhoneCodinh").val("");
                    $("#txtAddress").val("");
                    $("#txtContent").val("");

                    $("#GuiDonHang").prop("disabled", false);
                    $("span#thongbao").html("Bạn đã gửi thông tin thành công !!");
                } else {
                    $("#GuiDonHang").prop("disabled", false);
                    $("span#thongbao").html("Lỗi không gửi được thông tin !!");
                }
            },
            failure: function (response) {
                $("#GuiDonHang").prop("disabled", false);
                $("span#thongbao").html("Lỗi không gửi được thông tin !!");
            }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            navigation: true,
            navigationText: ["«", "»"],
            rewindNav: false,
            scrollPerPage: false,
            slideSpeed: 1500,
            pagination: false,
            paginationNumbers: false,
            autoPlay: false,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        function syncPosition(el) {
            var current = this.currentItem;
            $("#sync2")
              .find(".owl-item")
              .removeClass("synced")
              .eq(current)
              .addClass("synced")
            if ($("#sync2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#sync2").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
</script>

<script type="text/javascript">
    CloudZoom.quickStart();
</script>
