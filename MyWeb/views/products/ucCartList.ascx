﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCartList.ascx.cs" Inherits="MyWeb.views.products.ucCartList" %>

<div class="box-cart-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub"><% = MyWeb.Global.GetLangKey("cart") %></div>
    </div>
    <div class="body-cart">
        <table class="table-cart-list">
            <thead>
                <tr>
                    <th><%= MyWeb.Global.GetLangKey("cart_stt")%></th>
                    <th><%= MyWeb.Global.GetLangKey("cart_product")%></th>
                    <th><%= MyWeb.Global.GetLangKey("cart_amount")%></th>
                    <th><%= MyWeb.Global.GetLangKey("cart_price")%></th>
                    <th><%= MyWeb.Global.GetLangKey("cart_money")%></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <asp:Repeater ID="rptDanhsach" runat="server" OnItemCommand="rptDanhsach_ItemCommand">
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblstt" runat="server"></asp:Label></td>
                        <td style="text-align: left;">
                            <img class="view-img-cart" src="<%#DataBinder.Eval(Container.DataItem, "img")%>" />
                            <%#DataBinder.Eval(Container.DataItem, "proname")%></td>
                        <td>
                            <asp:TextBox ID="txtSL" class="txt-number-cart" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "number")%>' onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')"></asp:TextBox>
                            <asp:LinkButton ID="btnUpdate" runat="server" class="btn-update-cart" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Update"><span>Cập nhập</span></asp:LinkButton></td>
                        <td><%#String.Format("{0:N0} đ", double.Parse(Eval("price").ToString()))%></td>
                        <td><%#String.Format("{0:N0} vnđ", double.Parse(Eval("money").ToString()))%></td>
                        <td>
                            <asp:LinkButton ID="lbt2" runat="server" class="btn-delete-cart" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Delete"><span><%= MyWeb.Global.GetLangKey("cart_remove")%></span></asp:LinkButton></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="6" class="view-price"><%= MyWeb.Global.GetLangKey("cart_total_pay")%><span><asp:Literal ID="lblTongtien" runat="server"></asp:Literal></span></td>
            </tr>
        </table>
    </div>
    <div class="cartlist_button">
        <div class="button-order">
            <asp:Literal ID="ltrThanhToan" runat="server"></asp:Literal>
        </div>
        <div class="next-buy"><a href="/" rel="nofollow"><span><%= MyWeb.Global.GetLangKey("continues-order")%></span></a></div>
    </div>
</div>
<script type="text/javascript">
    function clickOrder() {
        alert('<%=strError %>');
    }
</script>
