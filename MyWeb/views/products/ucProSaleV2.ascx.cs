﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProSaleV2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = false;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            if (GlobalClass.viewProducts3 != "") { strNumberView = GlobalClass.viewProducts3; }
            if (strNumberView == "")
            {
                strNumberView = "5";
            }
            IEnumerable<tbProduct> product = db.tbProducts.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).Where(s => s.proActive == 1 && s.proBanchay == 1 && s.proLang.Trim() == lang);
            product = product.Skip(0).Take(int.Parse(strNumberView));
            if (product.Count() > 0)
            {                
                showAdv = true;

                //int countss = product.Count();
                //int i = 0;
                //foreach (tbProduct pro in product)
                //{

                //    _str += "<li class=\"item-list\">";
                //    if (pro.proImage.Length > 0)
                //    {
                //        _str += "<a class=\"img-view\" href=\"/" + pro.proTagName + ".html\" title=\"" + pro.proName.ToString() + "\"><img src=\"" + pro.proImage.Split(Convert.ToChar(","))[0] + "\" alt=\"" + pro.proName.ToString() + "\"/></a>";
                //    }
                //    else
                //    {
                //        _str += "<a class=\"img-view\" title=\"" + pro.proName.ToString() + "\" href=\"/" + pro.proTagName + ".html\"><img src=\"images/no_image.jpg\" alt=\"" + pro.proName.ToString() + "\"/></a>";
                //    }
                //    _str += "<h6><a class=\"link-view\" title=\"" + pro.proName.ToString() + "\" href=\"/" + pro.proTagName + ".html\">" + pro.proName.ToString() + "</a></h6>";
                //    _str += "</li>";
                //    i++;
                //}
            }
            //ltrProduct.Text = _str;
            ltrProduct.Text = common.LoadProductListV2(product);
        }
    }
}