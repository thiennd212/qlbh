﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.pages
{
    public partial class ucPageDetail : System.Web.UI.UserControl
    {
        string lang = "vi";
        string mid = "";
        string currentPage;
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        public string ur = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["hp"] != null) { mid = Request["hp"].ToString() + ".html"; ; }
            currentPage = Request.QueryString["page"] != null ? Request.QueryString["page"] : "1";
            try { Convert.ToInt16(currentPage); }
            catch { currentPage = "1"; }
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            ur = "http://" + Request.Url.Host + Request.Url.AbsolutePath;
            if (!IsPostBack)
            {
                BindData();
            }
        }
        void BindData()
        {
            string _str = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByPageLink(mid);
            if (list.Count > 0)
            {
                ltrName.Text = list[0].pagName;
                _str += list[0].pagDetail;
                BindAdv(list[0].pagId);
            }
            ltrModules.Text = _str;
        }
        protected void BindAdv(string pageId)
        {
            string strResult = "";
            var listAdv = db.tbAdvertise_Pages.Where(s => s.pageID == int.Parse(pageId)).ToList();
            if (listAdv.Count() > 0)
            {
                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                strResult += "<div class='advByPage' id='advPage'>";
                for (int i = 0; i < dsAdv.Count(); i++)
                {
                    strResult += "<div class='advItem'>";
                    strResult += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                    strResult += "</div>";
                }
                strResult += "</div>";
            }
            ltrAdv.Text = strResult;
        }
    }
}