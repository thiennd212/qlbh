﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsHomePriority.ascx.cs" Inherits="MyWeb.views.news.ucNewsHomePriority" %>

<div class="box-news-index">
    <div class="event-index clearfix">
        <asp:Literal ID="ltrEvent" runat="server"></asp:Literal>
        <div class="last-news">
            <div class="header">
                <%= MyWeb.Global.GetLangKey("news_latest_news")%>
                <span>
                    <asp:Literal ID="ltrDate" runat="server"></asp:Literal></span>
            </div>
            <asp:Literal ID="ltrLastNews" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="clearfix">
        <div class="topical">
            <asp:Literal ID="ltrTopical" runat="server"></asp:Literal></div>
    </div>
</div>
