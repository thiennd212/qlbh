﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucThematicNews : System.Web.UI.UserControl
    {
        string lang = "vi";
        string viewNews7 = "5";
        public bool showAdv = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            string _str = "";
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            list = tbNewsDB.tbNews_GetByTop(viewNews7, "newType=1 and newActive=0 and newLang='" + lang + "'", "newDate desc,newid desc");
            int i = 0;
            if (list.Count > 0)
            {
                for (i = 0; i < list.Count; i++)
                {
                    tbGroupNew gn = db.tbGroupNews.Where(s => s.grnId == int.Parse(list[i].grnID)).FirstOrDefault();
                    _str += "<div class='frame-top'>";
                    string link = "/" + list[i].newTagName + ".html";
                    if (list[i].newImage.Length > 0)
                    {
                        _str += string.Format("<a class=\"view-img\" href=\"{0}\" title=\"{2}\"><img src=\"{1}\" alt=\"{2}\" title=\"{2}\" /></a>", link, list[i].newImage, list[i].newName);
                    }
                    _str += string.Format("<a class=\"view-link\" href=\"{0}\" title=\"{1}\">{1}</a>", link, MyWeb.Common.StringClass.GetContent(list[i].newName), MyWeb.Common.StringClass.GetContent(list[i].newName));
                    _str += "</div>";
                }
            }
            else
            {
                showAdv = false;
            }
            list.Clear();
            list = null;
            ltrThematic.Text = _str;
        }
    }
}