﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsHot.ascx.cs" Inherits="MyWeb.views.news.ucNewsHot" %>

<%if (showAdv)
  {%>
<div class="box-news-hot">
    <div class="header"><%= MyWeb.Global.GetLangKey("news_new")%></div>
    <ul class="body-news">
        <asp:Literal ID="ltrNew" runat="server"></asp:Literal>
    </ul>
</div>

<%} %>

