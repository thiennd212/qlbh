﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucFeedbackClient : System.Web.UI.UserControl
    {
        string idcha = "";
        string grnid = "";
        string date = "";
        string lang = "vi";
        string p = "";
        string mem = "";
        string strWhere = "";
        int currentPage = 1;
        int recordCount = 0;
        int pageSize = 10;
        private bool fullContent = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            }
            catch { }
            try { pageSize = int.Parse(GlobalClass.viewNews1); }
            catch { }
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                ltrName.Text = "Ý kiến khách hàng";
                BindData();
            }
        }
        private string LoadWhere()
        {
            strWhere = "conActive = '1' and  conLang = '" + lang + "' ";
            List<tbContactDATA> list = tbContactDB.tbContact_GetByID(idcha);
            if (list.Count > 0)
            {
                Session["Lang"] = list[0].conLang.Trim().ToLower();
                if (GlobalClass.showSEO.Equals("true"))
                {
                    Page.Title = list[0].conName + " | " + GlobalClass.conTitle;
                }
                else
                {
                    Page.Title = list[0].conName;
                }

            }
            recordCount = int.Parse(tbContactDB.tbContacts_Count("Select Count(*) from  tbContact  Where " + strWhere));
            return strWhere;
        }
        private void BindData()
        {
            LoadWhere();
            strWhere = "conActive = '1' and  conLang = '" + lang + "' ";
            var list = db.tbContacts.Where(s => s.conActive == 1 && s.conLang == lang).OrderByDescending(s => s.conDate).ToList();
            list = list.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            //List<tbContactDATA> list = tbContactDB.tbContact_Paging((currentPage).ToString(), pageSize.ToString(), "*", strWhere, "conDate desc,conId desc");
            string _str = "";
            if (list.Count > 0)
            {
                _str += "<div class=\"box-body clearfix\">";
                for (int i = 0; i < list.Count; i++)
                {
                    string link = "/" + list[i].conwebsite + ".html";
                    _str += "<div class='cat-feed clearfix'>";
                    _str += "   <div class='cat-feedImg'><img src='" + list[i].confile + "' alt='" + list[i].conName + "' align='left'></div>  ";
                    _str += "   <div class='feednews'>  ";
                    _str += "       <i class='fa fa-quote-right'></i>";
                    string strReContent = list[i].conDetail.Length > 185 ? MyWeb.Common.StringClass.GetContent(list[i].conDetail.ToString(), 185) : list[i].conDetail.ToString();
                    _str += "       <div class='clearfix'>" + strReContent + "</div>";
                    _str += "       <h3 class='testimonial__name __web-inspector-hide-shortcut__'>" + list[i].conName + "</h3>";
                    _str += "       <h3 class='testimonial__job'>" + list[i].conAddress + "</h3>";
                    _str += "       <div class='date'>" + list[i].conDate.ToString().Substring(0, list[i].conDate.ToString().IndexOf(" ")) + "</div>";
                    _str += "   </div>";
                    _str += "</div>";
                }
                _str += "</div>";
                ltrlist.Text = _str;
            }
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }
    }
}