﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucShopCart : System.Web.UI.UserControl
    {
        public bool showCart = false;
        string lang = "vi";
        DataTable dsCart = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (Session["prcart"] != null) { dsCart = (DataTable)Session["prcart"]; } else { Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail(); dsCart = (DataTable)Session["prcart"]; }
            if (!IsPostBack)
            {
                if (GlobalClass.pageProducts)
                    BindCart();
            }
        }
        void BindCart()
        {
            showCart = true;
            Double NumberPrice = 0;
            if (dsCart.Rows.Count > 0)
            {
                for (int i = 0; i < dsCart.Rows.Count; i++)
                {
                    NumberPrice = NumberPrice + Convert.ToDouble(dsCart.Rows[i]["money"].ToString());
                }
                ltrNumber.Text = MyWeb.Global.GetLangKey("cart_total") + ": " + String.Format("{0:N0} " + GlobalClass.CurrencySymbol, NumberPrice);
                Session["prcart"] = dsCart;
            }
            else
            {
                ltrNumber.Text = MyWeb.Global.GetLangKey("cart_total") + " " + String.Format("{0:N0} " + GlobalClass.CurrencySymbol, NumberPrice);
            }
            ltrNumber.Text = "";
            ltrNumber.Text = MyWeb.Global.GetLangKey("total") + " " + LoadCart() + " " + MyWeb.Global.GetLangKey("products");
        }

        public string LoadCart()
        {
            if (System.Web.HttpContext.Current.Session["prcart"] != null)
            {
                DataTable cartdetail = (DataTable)System.Web.HttpContext.Current.Session["prcart"];
                if (cartdetail.Rows.Count > 0)
                {
                    string inumofproducts = "";
                    string totalvnd = "";
                    if (cartdetail.Rows.Count > 0)
                    {
                        int num2 = 0;
                        for (int i = 0; i < cartdetail.Rows.Count; i++)
                        {
                            num2 += Convert.ToInt32(cartdetail.Rows[i]["number"].ToString());
                        }
                        inumofproducts = num2.ToString();
                    }
                    return inumofproducts;
                }
                else
                {
                    return "0";
                }
            }
            else
            {
                return "0";
            }
        }
    }
}