﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MyWeb.views.control
{
    public partial class ucSearchLinkOrder : System.Web.UI.UserControl
    {
        private string inputValue = MyWeb.Global.GetLangKey("search_text");
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrSearchOrder.Text = "<a href=\"" + ConfigurationManager.AppSettings["url-web-base"] + "/tra-cuu-don-hang.html\" title=\"Tra cứu đơn hàng\">TRA CỨU ĐƠN HÀNG</a>";
        }
    }
}