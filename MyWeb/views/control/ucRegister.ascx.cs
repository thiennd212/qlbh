﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucRegister : System.Web.UI.UserControl
    {
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            tbMembersDATA ObjtbMembersDATA = new tbMembersDATA();
            ObjtbMembersDATA.uID = common.killChars(txtUser.Text);
            ObjtbMembersDATA.uPas = vmmsclass.Encodingvmms.Encode(txtpass.Text);
            ObjtbMembersDATA.Email = common.killChars(txtmail.Text);
            ObjtbMembersDATA.Add = common.killChars(txtdiachi.Text);
            ObjtbMembersDATA.Tell = common.killChars(txtDienthoai.Text);
            ObjtbMembersDATA.Fax = common.killChars(txtHoten.Text);
            List<tbMember> lstU = db.tbMembers.Where(s => s.uID == txtUser.Text).ToList();
            List<tbMember> lstE = db.tbMembers.Where(s => s.Email == txtmail.Text).ToList();
            if (lstU.Count > 0)
            {
                lblthongbao.Text = MyWeb.Global.GetLangKey("user_register_user_error");
                string rqurl = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Write("<script>alert('Tài khoản đã tồn tại!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");

            }
            else if (lstE.Count > 0)
            {
                lblthongbao.Text = MyWeb.Global.GetLangKey("user_register_email_error");
            }
            else if (tbMembersDB.tbMembers_Add(ObjtbMembersDATA))
            {
                txtUser.Text = "";
                txtpass.Text = "";
                txtpass_confirm.Text = "";
                txtmail.Text = "";
                txtDienthoai.Text = "";
                txtdiachi.Text = "";
                txtHoten.Text = "";
                Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_register_success") + "'); window.location='/dang-nhap.html'</script>");

            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtUser.Text = "";
            txtpass.Text = "";
            txtpass_confirm.Text = "";
            txtmail.Text = "";
            txtDienthoai.Text = "";
            txtdiachi.Text = "";
            txtHoten.Text = "";
        }
    }
}