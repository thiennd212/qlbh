﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucNewsLetter : System.Web.UI.UserControl
    {
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnNewsLetter_Click(object sender, EventArgs e)
        {
            tbNewsletter obj = new tbNewsletter();
            obj.Email = txtEmail.Text;
            obj.letterName = "Khách";
            db.tbNewsletters.InsertOnSubmit(obj);
            db.SubmitChanges();

            btnErr.Visible = true;
            btnErr.Text = "<p class=\"news-letter-err\">" + MyWeb.Global.GetLangKey("news_letter_err") + "</p>";
        }
    }
}