﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChangeUser.ascx.cs" Inherits="MyWeb.views.control.ucChangeUser" %>

<div class="box-change-user">
    <div class="cate-header">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("user_profile") %></div>
    </div>
    <div class="box-body-sub">
        <div class="row">
            <div class="form-group">
                <span class="view-messenger">
                    <asp:Literal ID="lblthongbao" runat="server"></asp:Literal></span>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_name")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtHoten" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtHoten" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_username")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtUser" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUser" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Email:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_address")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtdiachi" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_mobilephone")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("user_profile_update") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("contact_cancel")%></asp:LinkButton>
                </div>
            </div>

        </div>
    </div>
</div>