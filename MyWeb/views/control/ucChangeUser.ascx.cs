﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucChangeUser : System.Web.UI.UserControl
    {
        string strUser = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["um"] != null)
            {
                strUser = Session["um"].ToString();
            }
            else
            {
                Response.Redirect("/");
            }
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            tbMember obj = db.tbMembers.Where(s => s.uID == strUser).FirstOrDefault();
            txtUser.Text = obj.uID;
            txtmail.Text = obj.Email;
            txtdiachi.Text = obj.Add;
            txtDienthoai.Text = obj.Tell;
            txtHoten.Text = obj.Fax;
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            List<tbMembersDATA> list = tbMembersDB.tbMembers_GetByUser(strUser);
            if (list.Count > 0)
            {
                list[0].ID = list[0].ID;
                list[0].uID = common.killChars(txtUser.Text);
                list[0].uPas = list[0].uPas;
                list[0].Email = common.killChars(txtmail.Text);
                list[0].Add = common.killChars(txtdiachi.Text);
                list[0].Tell = common.killChars(txtDienthoai.Text);
                list[0].Fax = common.killChars(txtHoten.Text);
                if (tbMembersDB.tbMembers_Update(list[0]))
                {
                    string rqurl = HttpContext.Current.Request.Url.AbsoluteUri;

                    Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_profile_update_success") + "')</script>");
                    Response.Write("<script>window.location='" + rqurl + "'</script>");
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("/");
        }
    }
}