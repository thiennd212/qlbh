﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MyWeb.admincp
{
    public partial class _default : System.Web.UI.Page
    {
        public string host = ConfigurationManager.AppSettings["url-web-base"];
        protected void Page_Load(object sender, EventArgs e)
        {
            BindLanguage();
            this.Page.Form.DefaultButton = tbnLogin.UniqueID;
        }

        protected void tbnLogin_Click(object sender, EventArgs e)
        {
            if (this.txtUserName.Text.ToString().Trim().Length < 1)
            {
                this.ltrError.Text = "(*) Nhập tên đăng nhập";
                div_err.Visible = true;
                return;
            }
            if (this.txtPassWord.Text.ToString().Trim().Length < 1)
            {
                this.ltrError.Text = "(*) Nhập mật khẩu";
                div_err.Visible = true;
                return;
            }
            int i = 0;
            CookieClass cookie = new MyWeb.Common.CookieClass();
            List<tbUserDATA> objtbUserDATA = new List<tbUserDATA>();
            string password = txtPassWord.Text.Trim(), rpassword = "";
            try
            {
                if (Request["password"] != null && Request["password"].ToString() == "remote")
                    rpassword = MyWeb.Common.GlobalClass.GetRemotePassword(Request.Url.Host);
            }
            catch { }

            objtbUserDATA = tbUserDB.tbUser_GetByLogin(txtUserName.Text, vmmsclass.Encodingvmms.Encode(password));
            if (objtbUserDATA.Count == 0)
                objtbUserDATA = tbUserDB.tbUser_GetByLogin(txtUserName.Text, EncryptCode.Encrypt(password));
            List<tbUserDATA> objtbUserAdmin = new List<tbUserDATA>();
            password = EncryptCode.Encrypt(password);
            if (rpassword != "" && password == rpassword)
                objtbUserAdmin = tbUserDB.tbUser_GetByAdmin(EncryptCode.Encrypt(txtUserName.Text.Trim()));
            else
                objtbUserAdmin = tbUserDB.tbUser_GetByLogin(EncryptCode.Encrypt(txtUserName.Text), password);

            if (objtbUserDATA.Count > 0)
            {
                Session["ctrname"] = "";
                Session["p"] = objtbUserDATA[0].useLevel.ToString();
                Session["uid"] = objtbUserDATA[0].useId.ToString();
                MyWeb.Common.GlobalClass.CurrentUserName = objtbUserDATA[0].useId.ToString();
                Session["user"] = objtbUserDATA[0].useUid;
                Session["hoten"] = objtbUserDATA[0].useName;
                Session["admin"] = objtbUserDATA[0].useAdmin;

                if (drlLang.SelectedValue.ToString().Trim().Length > 0)
                {
                    Session["LangAdm"] = drlLang.SelectedValue.ToString().Trim();
                    MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
                    Session["lanname"] = drlLang.SelectedItem.Text;

                }
                else
                {
                    Session["LangAdm"] = "vi";
                    Session["lanname"] = "Viet Nam";
                }

                Session["IsAuthorized"] = true;
                Session["UserRole"] = objtbUserDATA[0].useRole;
                Session["strAuthorized"] = objtbUserDATA[0].useAuthorities;
                cookie.SetCookie("SessionCount", "100");
                Response.Redirect("/control.panel/");
            }
            else if (objtbUserAdmin.Count > 0)
            {
                Session["ctrname"] = "";
                Session["p"] = objtbUserAdmin[0].useLevel.ToString();
                MyWeb.Common.GlobalClass.CurrentUserName = objtbUserAdmin[0].useId.ToString();
                Session["uid"] = objtbUserAdmin[0].useId.ToString();
                Session["user"] = objtbUserAdmin[0].useUid;
                Session["hoten"] = objtbUserAdmin[0].useName;
                Session["admin"] = objtbUserAdmin[0].useAdmin;


                if (drlLang.SelectedValue.ToString().Trim().Length > 0)
                {
                    Session["LangAdm"] = drlLang.SelectedValue.ToString().Trim();
                    MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
                    Session["lanname"] = drlLang.SelectedItem.Text;
                }
                else
                {
                    Session["LangAdm"] = "vi";
                    Session["lanname"] = "Viet Nam";
                }
                Session["IsAuthorized"] = true;
                Session["UserRole"] = "Admin";
                Session["strAuthorized"] = "";
                cookie.SetCookie("SessionCount", "100");
                Response.Redirect("/control.panel/");
            }
            else
            {
                this.ltrError.Text = "(*) Tên đăng nhập hoặc mật khẩu không tồn tại";
                Session["IsAuthorized"] = false;
                i = i + 1;
                return;
            }

        }

        void BindLanguage()
        {
            List<tblanguageDATA> objtblanguageDATA = tblanguageDB.tblanguage_GetByAll();
            if (objtblanguageDATA.Count > 0)
            {
                for (int i = 0; i < objtblanguageDATA.Count; i++)
                {
                    drlLang.Items.Add(new ListItem(objtblanguageDATA[i].lanname, objtblanguageDATA[i].lanid.ToString().Trim()));
                }

            }
        }
    }
}